//@ts-nocheck
import prisma from "../lib/prisma";

import { SessionType } from "@/types/types";

export default async function updateUser(session: SessionType) {
  let user = await prisma.user.findUnique({
    where: {
      username: session.username,
    },
  });

  const { username, firstname, lastname, email, amivId } = session;

  if (!user) {
    user = await prisma.user.create({
      data: {
        username,
        firstname,
        lastname,
        email,
        amivId,
      },
    });
  } else if (!user.firstname) {
    user = await prisma.user.update({
      where: {
        username,
      },
      data: {
        firstname,
        lastname,
        email,
        amivId,
      },
    });
  }

  return user;
}
