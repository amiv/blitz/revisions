import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import localizedFormat from "dayjs/plugin/localizedFormat";
import duration from "dayjs/plugin/duration";
import "dayjs/locale/de-ch";
import "dayjs/locale/en-gb";
dayjs.extend(relativeTime);
dayjs.extend(localizedFormat);
dayjs.extend(duration);

const localeMapping = {
  de: "de-ch",
  en: "en-gb",
};

export function isInFuture(dateStr: string) {
  const date = dayjs(dateStr);
  return date > dayjs();
}

export function formatDateTime(dateStr: string, locale: string) {
  const localeMapping = {
    de: "de-ch",
    en: "en-gb",
  };
  const fullLocale = locale ? localeMapping[locale as "de" | "en"] : "en-gb";
  dayjs.locale(fullLocale);

  return `${dayjs(dateStr)
    .locale(fullLocale)
    .format(
      `${locale === "de" ? "dd" : "ddd"}, L LT`,
    )} (${dayjs().to(dayjs(dateStr))})`;
}

export function formatDate(dateStr: string, locale: string) {
  const localeMapping = {
    de: "de-ch",
    en: "en-gb",
  };

  const fullLocale = locale ? localeMapping[locale as "de" | "en"] : "en-gb";
  dayjs.locale(fullLocale);

  return `${dayjs(dateStr)
    .locale(fullLocale)
    .format(
      `${locale === "de" ? "dd" : "ddd"}, L`,
    )} (${dayjs().to(dayjs(dateStr))})`;
}

export function getRelativeTime(to: Date, locale: string) {
  //return dayjs.duration(dayjs().diff(to)).locale(locale ? localeMapping[locale as "de" | "en"] : "en-gb").humanize();
  dayjs.locale(locale ? localeMapping[locale as "de" | "en"] : "en-gb");
  return dayjs().to(dayjs(to));
}
