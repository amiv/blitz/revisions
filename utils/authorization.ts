import type { UserType, SessionType } from "@/types/types";
import auth from "@/lib/auth";
import { useSession } from "next-auth/react";

type GroupsType =
  | "ANON"
  | "LOGGEDIN"
  | "MEMBER"
  | "AUTHOR"
  | "OWNER"
  | "LECTOR"
  | "LAYOUT"
  | "ADMIN";
interface PermissionsType {
  [index: string]: GroupsType[];
}

const permissions: PermissionsType = {
  "issue.view": ["ANON"],
  "issue.create": ["ADMIN"],
  "issue.edit": ["ADMIN"],
  "issue.export": ["LAYOUT"],
  "article.view": ["MEMBER"],
  "article.create": ["AUTHOR"],
  "article.edit": ["OWNER", "ADMIN"],
  "article.lectorate": ["LECTOR"],
  "article.layout": ["LAYOUT"],
  "article.move": ["OWNER", "ADMIN"],
  "article.delete": ["OWNER", "ADMIN"],
  "user.view": ["LOGGEDIN"],
  "users.view": ["ADMIN"],
  "users.edit": ["ADMIN"],
} as const;

const funcs = {
  ANON: (
    session?: SessionType | null,
    user?: UserType,
    owner?: string,
  ): boolean => {
    return true;
  },
  LOGGEDIN: (
    session?: SessionType | null,
    user?: UserType,
    owner?: string,
  ): boolean => {
    return !!session;
  },
  MEMBER: (
    session?: SessionType | null,
    user?: UserType,
    owner?: string,
  ): boolean => {
    return /(AUTHOR|LECTOR|LAYOUT)/.test(user?.roles || "");
  },
  AUTHOR: (
    session?: SessionType | null,
    user?: UserType,
    owner?: string,
  ): boolean => {
    return !!user?.roles?.split(",").includes("AUTHOR");
  },
  OWNER: (
    session?: SessionType | null,
    user?: UserType,
    owner?: string,
  ): boolean => {
    // !!user?.username prevents undefined === undefined
    return (
      (user?.username === owner && !!user?.username) ||
      (session?.username === owner && !!session?.username)
    );
  },
  LECTOR: (
    session?: SessionType | null,
    user?: UserType,
    owner?: string,
  ): boolean => {
    return !!user?.roles?.split(",").includes("LECTOR");
  },
  LAYOUT: (
    session?: SessionType | null,
    user?: UserType,
    owner?: string,
  ): boolean => {
    return !!user?.roles?.split(",").includes("LAYOUT");
  },
  ADMIN: (
    session?: SessionType | null,
    user?: UserType,
    owner?: string,
  ): boolean => {
    return !!session?.isAdmin;
  },
} as const;

const isPartOfGroup = (
  which: GroupsType,
  session?: SessionType | null,
  user?: UserType,
  owner?: string,
): boolean => {
  return funcs[which](session, user, owner);
};

const isAllowed = (
  target: string,
  session?: SessionType | null,
  user?: UserType,
  owner?: string,
): boolean => {
  const allowedRoles = permissions[target];
  return allowedRoles
    .map((role: GroupsType) => isPartOfGroup(role, session, user, owner))
    .reduce((acc: boolean, current: boolean) => acc || current, false);
};

const isAllowedClient = (
  target: string,
  user?: UserType,
  owner?: string,
): boolean => {
  const { data: session } = useSession();

  return isAllowed(target, session || undefined, user, owner);
};

const isAllowedServer = async (
  target: string,
  user?: UserType,
  owner?: string,
): Promise<boolean> => {
  const session = await auth();

  return isAllowed(target, session, user, owner);
};

export { isAllowed, isAllowedClient, isAllowedServer };
