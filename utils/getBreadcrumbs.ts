// @ts-ignore
import prisma from "@/lib/prisma";

interface BreadcrumbType {
  label: string;
  path: string;
  translate: boolean;
}

export default async function getBreadcrumbs(
  pathname: string,
): Promise<BreadcrumbType[]> {
  if (!pathname) return [];
  const path = pathname.replace(/^\/(en|de)/, "");
  let breadcrumbs = [];

  if (/^(\/issue\/[0-9]+(\/(article|edit)\/[0-9]+)?)?$/.test(path)) {
    breadcrumbs.push({
      label: "home",
      path: "/",
      translate: true,
    });
    // main
    let id;
    if (/\/issue\//.test(path)) {
      id = path.replace(/^\/issue\//, "").split("/")[0];
      // @ts-ignore
      const issue = await prisma.issue.findUnique({
        where: {
          id: Number(id),
        },
        select: {
          name: true,
        },
      });
      breadcrumbs.push({
        label: issue.name,
        path: `/issue/${id}`,
        translate: false,
      });
    }

    if (/\/(article|edit)\//.test(path)) {
      const artId = path
        .replace(/^\/issue\/[0-9]+\/(article|edit)\//, "")
        .split("/")[0];
      // @ts-ignore
      const article = await prisma.article.findUnique({
        where: {
          id: Number(artId),
        },
        select: {
          title: true,
        },
      });
      if (!article) return [];
      breadcrumbs.push({
        label: article.title,
        path: `/issue/${id}/article/${artId}`,
        translate: false,
      });
    }
  } else if (/^\/admin/.test(path)) {
    breadcrumbs.push({
      label: "administration",
      path: "/admin",
      translate: true,
    });
  }

  return breadcrumbs;
}
