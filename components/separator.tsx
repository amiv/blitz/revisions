import blitz from "@/app/icon.svg";

export default function Separator() {
  return (
    <div className="relative mx-auto flex w-full max-w-[1000px] items-center py-10">
      <div className="flex-grow border-t border-gray-400 dark:border-gray-500" />
      <img className="mx-4 block w-8 dark:hidden" src="/signet.svg" />
      <img className="mx-4 hidden w-8 dark:block" src="/signet-white.svg" />
      <div className="flex-grow border-t border-gray-400 dark:border-gray-500" />
    </div>
  );
}
