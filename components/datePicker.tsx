import DatePicker from "react-date-picker";
import "react-date-picker/dist/DatePicker.css";
import "react-calendar/dist/Calendar.css";

import { useCurrentLocale } from "@/locales/client";

import { IconCalendar, IconX } from "@tabler/icons-react";

import type { Value } from "@/types/types";

export default function CustomDatePicker({
  date,
  setDate,
}: {
  date: Value;
  setDate: (value: Value) => void;
}) {
  const locale = useCurrentLocale();

  const localeMapping = {
    de: "de-ch",
    en: "en-gb",
  };

  return (
    <DatePicker
      required={true}
      showLeadingZeros={true}
      onChange={setDate}
      value={date}
      calendarClassName="dark:!bg-stone-700 p-4 shadow-md rounded-lg !border-none"
      className="w-full border-b border-stone-600 dark:border-stone-400 [&>div]:border-none"
      calendarIcon={<IconCalendar stroke={1.5} />}
      clearIcon={<IconX stroke={1.5} />}
      locale={localeMapping[locale || "en"]}
    />
  );
}
