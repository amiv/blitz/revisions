"use client";

import { usePathname } from "next/navigation";

import NavLinksDesktop from "./navLinksDesktop";
import MobileNav from "./mobileNav";

import { gql, useQuery } from "@apollo/client";

const getBreadcrumbsQuery = gql`
  query getBreadcrumbs($pathname: String) {
    getBreadcrumbs(pathname: $pathname) {
      label
      path
      translate
    }
  }
`;

export default function NavLinks({
  navItems,
}: {
  navItems: { label: string; path: string; match: string }[];
}) {
  const pathname = usePathname();
  const { data: breadcrumbs } = useQuery(getBreadcrumbsQuery, {
    variables: {
      pathname,
    },
  });

  return (
    <>
      <NavLinksDesktop
        navItems={navItems}
        breadcrumbs={breadcrumbs?.getBreadcrumbs}
      />
      <MobileNav
        navItems={navItems}
        breadcrumbs={breadcrumbs?.getBreadcrumbs}
      />
    </>
  );
}
