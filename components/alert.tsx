import { ReactNode } from "react";

export default function Alert({
  title,
  icon,
  className = "",
  error = false,
  children,
}: {
  title: string;
  icon: ReactNode;
  className?: string;
  error?: boolean;
  children: ReactNode;
}) {
  return (
    <div
      className={`${className} rounded-md p-6 text-left shadow-md ${error ? "bg-red-600 text-white" : "bg-stone-100 text-black dark:bg-stone-700 dark:text-stone-50"}`}
    >
      <div className="flex gap-6">
        <div>{icon}</div>
        <div>
          <p className="font-bold">{title}</p>
          <p className="mt-2">{children}</p>
        </div>
      </div>
    </div>
  );
}
