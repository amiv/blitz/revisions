export default function Unauthorized({ target }: { target?: string }) {
  return (
    <div className="my-8 text-center text-black dark:text-white">
      <h1 className="text-2xl font-bold sm:text-3xl">401 - Unauthorized</h1>

      <p className="mt-4">
        You don&apos;t have the permissions{" "}
        {target && (
          <>
            <span>for </span>
            <span className="font-mono font-bold">{target}</span>
          </>
        )}{" "}
        required to view this page
      </p>
    </div>
  );
}
