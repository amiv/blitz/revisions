"use client";

import Link from "next/link";

import { Menu } from "@headlessui/react";

import { useChangeLocale } from "@/locales/client";

import { IconChevronDown, IconChevronUp } from "@tabler/icons-react";

export default function LanguageMenu({ locale }: { locale: string }) {
  const locales: ("de" | "en")[] = ["de", "en"];
  const changeLocale = useChangeLocale();
  //const changeLocale = (l: "de" | "en") => {};

  return (
    <Menu>
      {({ open }) => (
        <>
          <Menu.Button className="flex items-center">
            <span>{locale.toUpperCase()}</span>
            {open ? (
              <IconChevronUp className="ml-2" />
            ) : (
              <IconChevronDown className="ml-2" />
            )}
          </Menu.Button>
          <Menu.Items className="absolute top-16 w-48 rounded-md bg-black bg-white p-2 text-lg shadow-md dark:bg-stone-700">
            {locales.map((l, i) => (
              <Menu.Item key={i}>
                {({ active }) => (
                  <button
                    className={`${l === locale && "bg-black font-semibold text-white dark:bg-stone-100 dark:text-black"} ${active && l !== locale && "bg-stone-100 dark:bg-stone-600"} w-full rounded-md p-2 text-left`}
                    onClick={() => changeLocale(l)}
                  >
                    {l.toUpperCase()}
                  </button>
                )}
              </Menu.Item>
            ))}
          </Menu.Items>
        </>
      )}
    </Menu>
  );
}
