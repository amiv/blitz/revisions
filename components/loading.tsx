export default function Loading() {
  return (
    <div className="text-center">
      <img
        src="/signet.svg"
        style={{ animation: "spin 3s ease-in-out infinite" }}
        className="h-3624mx-auto block w-24 drop-shadow-md dark:hidden"
        alt="Spinning blitz logo"
      />
      <img
        src="/signet-white.svg"
        style={{ animation: "spin 3s ease-in-out infinite" }}
        className="mx-auto hidden h-24 w-24 drop-shadow-md dark:block"
        alt="Spinning blitz logo"
      />
      <p className="pt-4 text-4xl font-black">Loading...</p>
    </div>
  );
}
