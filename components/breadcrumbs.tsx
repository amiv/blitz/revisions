import Link from "next/link";

import { IconChevronRight } from "@tabler/icons-react";

import { useI18n } from "@/locales/client";

export default function Breadcrumbs({
  items,
}: {
  items: { label: string; path: string; translate: boolean }[] | undefined;
}) {
  const t = useI18n();

  if (!items) return <></>;

  return (
    <ul className="flex items-center overflow-x-auto pb-2 text-stone-600 dark:text-stone-400">
      {items.map((item, i) => (
        <li key={i} className="flex items-center">
          {i !== 0 && <IconChevronRight size={16} />}
          <Link href={item.path} className="mx-2 whitespace-nowrap">
            {
              // @ts-ignore
              item.translate ? t(item.label) : item.label
            }
          </Link>
        </li>
      ))}
    </ul>
  );
}
