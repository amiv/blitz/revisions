import type { ReactNode } from "react";

export default function title({ children }: { children: ReactNode }) {
  return (
    <div className="relative flex min-h-96 w-full flex-col justify-center text-center shadow-sm dark:border-b dark:border-stone-700">
      <div
        className="bg-grid-slate-900 absolute inset-0 -z-10 block min-h-96 dark:hidden"
        style={{
          backgroundSize: "20px 20px",
          backgroundImage:
            "radial-gradient(circle, #cccccc 1px, rgba(0, 0, 0, 0) 1px)",
          maskImage: "linear-gradient(to bottom, transparent, black)",
          WebkitMaskImage: "linear-gradient(to bottom, transparent, black)",
        }}
      />
      <div
        className="bg-grid-slate-900 absolute inset-0 -z-10 hidden min-h-96 dark:block"
        style={{
          backgroundSize: "20px 20px",
          backgroundImage:
            "radial-gradient(circle, #444444 1px, rgba(0, 0, 0, 0) 1px)",
          maskImage: "linear-gradient(to bottom, transparent, black)",
          WebkitMaskImage: "linear-gradient(to bottom, transparent, black)",
        }}
      />
      <div />
      <div className="mx-auto w-full max-w-[1200px] px-6 py-12 text-black dark:text-stone-100">
        {children}
      </div>
    </div>
  );
}
