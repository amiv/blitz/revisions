"use client";

import { useState } from "react";

import { useSession } from "next-auth/react";

import { IconPlus, IconRocket, IconX } from "@tabler/icons-react";
import { useI18n, useCurrentLocale } from "@/locales/client";

import Modal from "@/components/modal";
import DatePicker from "@/components/datePicker";
import Alert from "@/components/alert";

import type { Value } from "@/types/types";

import { gql, useMutation } from "@apollo/client";

const addIssueMutation = gql`
  mutation addIssue(
    $title: String
    $semester: String
    $number: String
    $publicationDate: DateTime
    $deadline: DateTime
  ) {
    addIssue(
      title: $title
      semester: $semester
      number: $number
      publicationDate: $publicationDate
      deadline: $deadline
    ) {
      status
      message
    }
  }
`;

export default function AddIssue({ refetch }: { refetch: () => void }) {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [title, setTitle] = useState<string>("");
  const [semester, setSemester] = useState<string>("");
  const [number, setNumber] = useState<string>("");
  const [publicationDate, setPublicationDate] = useState<Value>(new Date());
  const [deadline, setDeadline] = useState<Value>(new Date());
  const [errors, setErrors] = useState<string[]>([]);
  const [serverError, setServerError] = useState<string>("");

  const [addIssue] = useMutation(addIssueMutation);

  const t = useI18n();
  const { data: session } = useSession();

  const displayValidation = (i: number) => {
    if (errors.length !== 5) return <></>;

    const msg = errors[i];
    return msg ? (
      <p className="text-sm text-red-600 dark:text-red-400">{msg}</p>
    ) : (
      <></>
    );
  };

  const validate = () => {
    let status = true;
    let err = [];

    if (title === "") {
      err.push(t("errTitle"));
      status = false;
    } else err.push("");
    if (!/^[FH]S[0-9][0-9]$/.test(semester)) {
      err.push(t("errSemester"));
      status = false;
    } else err.push("");
    if (number.length !== 2 || !/^[0-9][0-9]$/.test(number)) {
      err.push(t("errNumber"));
      status = false;
    } else err.push("");
    if (!publicationDate) {
      err.push(t("errPublicationDate"));
      status = false;
    } else err.push("");
    if (!deadline) {
      err.push(t("errDeadline"));
      status = false;
    } else err.push("");

    setErrors(err);

    return status;
  };

  const handleSubmit = async () => {
    if (!validate()) return;

    const res = await addIssue({
      variables: {
        title,
        semester,
        number,
        publicationDate,
        deadline,
      },
    });

    if (res.data.addIssue.status === "OK") {
      refetch();
      setTitle("");
      setSemester("");
      setNumber("");
      setPublicationDate(new Date());
      setDeadline(new Date());
      setIsOpen(false);
      setServerError("");
    } else {
      setServerError(res.data.addIssue.message);
    }
  };

  if (!session?.isAdmin) return <></>;

  return (
    <>
      <button
        onClick={() => setIsOpen(true)}
        className="mx-auto mt-8 flex gap-2 rounded-md bg-black px-3 py-2 font-semibold text-white shadow-md hover:bg-stone-800 dark:bg-stone-100 dark:text-black dark:hover:bg-stone-200"
      >
        <IconPlus />
        <span>{t("addIssue")}</span>
      </button>
      <Modal
        open={isOpen}
        onClose={() => setIsOpen(false)}
        title={t("addIssue")}
        description={t("addIssueDescription")}
      >
        <div className="mt-4 grid grid-cols-1 gap-4 sm:grid-cols-2">
          <div className="col-span-full">
            <label>
              <p className="text-sm text-stone-600 dark:text-stone-400">
                {t("issueTitle")}
              </p>
              <input
                type="text"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
                placeholder={t("issueTitlePlaceholder")}
                className="w-full border-b border-stone-600 bg-white p-1 dark:border-stone-400 dark:bg-stone-700"
              />
              {displayValidation(0)}
            </label>
          </div>
          <div className="col-span-1">
            <label>
              <p className="text-sm text-stone-600 dark:text-stone-400">
                {t("issueSemester")}
              </p>
              <input
                type="text"
                value={semester}
                onChange={(e) => setSemester(e.target.value)}
                placeholder={t("issueSemesterPlaceholder")}
                className="w-full border-b border-stone-600 bg-white p-1 dark:border-stone-400 dark:bg-stone-700"
              />
              {displayValidation(1)}
            </label>
          </div>
          <div className="col-span-1">
            <label>
              <p className="text-sm text-stone-600 dark:text-stone-400">
                {t("issueNumber")}
              </p>
              <input
                type="text"
                value={number}
                onChange={(e) => setNumber(e.target.value)}
                placeholder={t("issueNumberPlaceholder")}
                className="w-full border-b border-stone-600 bg-white p-1 dark:border-stone-400 dark:bg-stone-700"
              />
              {displayValidation(2)}
            </label>
          </div>
          <div className="col-span-1">
            <label>
              <p className="text-sm text-stone-600 dark:text-stone-400">
                {t("issuePublicationDate")}
              </p>
              <DatePicker date={publicationDate} setDate={setPublicationDate} />
              {displayValidation(3)}
            </label>
          </div>
          <div className="col-span-1">
            <label>
              <p className="text-sm text-stone-600 dark:text-stone-400">
                {t("issueDeadline")}
              </p>
              <DatePicker date={deadline} setDate={setDeadline} />
              {displayValidation(4)}
            </label>
          </div>
          {serverError && (
            <div className="col-span-full mt-4">
              <Alert
                title={t("serverError")}
                icon={<IconX stroke={1.5} />}
                error={true}
              >
                {serverError}
              </Alert>
            </div>
          )}
          <div className="col-span-full mt-4">
            <button
              onClick={handleSubmit}
              className="mx-auto flex gap-2 rounded-md bg-black px-3 py-2 font-semibold text-white shadow-md hover:bg-stone-800 dark:bg-white dark:text-black hover:dark:bg-stone-200"
            >
              <IconRocket stroke={1.5} />
              <span>{t("addIssue")}</span>
            </button>
          </div>
        </div>
      </Modal>
    </>
  );
}
