import Link from "next/link";

import { IconArrowRight } from "@tabler/icons-react";

import { formatDateTime, formatDate } from "@/utils/dates";

import { IssueType } from "@/types/types";

function is_server() {
  return !(typeof window != "undefined" && window.document);
}

export default function NextIssueCard({
  issue,
  locale,
  showLogo = false,
  showViewButton = true,
  showArticles = false,
  showNextIssue = false,
  t,
}: {
  issue: IssueType;
  locale: string;
  showLogo?: boolean;
  showViewButton?: boolean;
  showArticles?: boolean;
  showNextIssue?: boolean;
  t: any;
}) {
  return (
    <div
      className={`relative grid w-full grid-cols-1 rounded-xl bg-white p-6 text-black shadow-xl sm:grid-cols-4 dark:bg-stone-700 dark:text-stone-50 ${showLogo ? "mt-16 pt-12 sm:pt-6" : "mt-4"}`}
    >
      {showLogo && (
        <div className="absolute -top-12 w-full sm:-top-16">
          <img
            src="/signet.svg"
            className="mx-auto block w-24 drop-shadow-md sm:w-32 dark:hidden"
            alt="blitz Signet"
          />
          <img
            src="/signet-white.svg"
            className="mx-auto hidden w-24 drop-shadow-md sm:w-32 dark:block"
            alt="blitz Signet"
          />
        </div>
      )}
      <div className="col-span-3">
        <div className="text-center sm:text-left">
          {showNextIssue && (
            <p className="text-xl font-semibold text-stone-600 dark:text-stone-400">
              {t("nextIssue")}
            </p>
          )}
          <h1 className="text-4xl font-black">{issue.name}</h1>
          <p className="text-xl font-semibold text-stone-600 dark:text-stone-400">
            blitz {issue.number}, {issue.semester}
          </p>
        </div>

        <div
          className={`grid w-fit grid-cols-1 gap-1 text-left font-semibold text-stone-600 sm:grid-cols-2 dark:text-stone-400 ${showArticles && issue.articles.length > 0 ? "py-4" : "pt-4"}`}
        >
          <p>{t("releaseDate")}:</p>
          <p>{formatDate(issue.publicationDate, locale)}</p>
          <p className="mt-2 sm:mt-0">{t("articleDeadline")}:</p>
          <p>{formatDateTime(issue.deadline, locale)}</p>
        </div>

        {showArticles &&
          issue.articles.map((art, i) => (
            <p
              className="hidden text-stone-600 sm:inline-block dark:text-stone-400"
              key={i}
            >
              {art.author && <span className="mr-2">{art.author}: </span>}
              <span>{art.title}</span>
            </p>
          ))}
      </div>
      {showViewButton && (
        <div className="mt-4 flex items-center justify-center sm:mt-0 sm:justify-end">
          <Link
            href={`/issue/${issue.id}`}
            className={`inline-flex h-fit items-center gap-4 rounded-md bg-black font-semibold text-white shadow-lg hover:bg-stone-800 dark:bg-white dark:text-black dark:hover:bg-stone-200 ${showLogo ? "px-6 py-4 text-2xl" : "text-md px-3 py-2"}`}
          >
            <span>{t("view")}</span>
            <IconArrowRight />
          </Link>
        </div>
      )}
    </div>
  );
}
