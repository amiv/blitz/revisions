"use client";

import Link from "next/link";

import { Menu } from "@headlessui/react";

import { signIn, signOut, useSession, SessionProvider } from "next-auth/react";

import {
  IconLock,
  IconUserCircle,
  IconChevronDown,
  IconChevronUp,
  IconUser,
  IconLogout,
} from "@tabler/icons-react";

import { useI18n } from "@/locales/client";

import { SessionType } from "@/types/types";

export default function SessionHandler({
  session,
}: {
  session: SessionType | null;
}) {
  const t = useI18n();

  return (
    <>
      {session ? (
        <Menu>
          {({ open }) => (
            <>
              <Menu.Button className="flex items-center">
                <IconUserCircle
                  stroke={1.5}
                  className="hidden w-6 shrink-0 sm:block"
                />
                <IconUserCircle
                  stroke={1.5}
                  className="block w-6 shrink-0 sm:hidden"
                />
                <p className="hidden shrink-0 pl-3 sm:block">{`${session.firstname[0]}. ${session.lastname}`}</p>
                {open ? (
                  <IconChevronUp className="ml-2 w-6 shrink-0" stroke={1.5} />
                ) : (
                  <IconChevronDown className="ml-2 w-6 shrink-0" stroke={1.5} />
                )}
              </Menu.Button>
              <Menu.Items className="w-content absolute top-16 z-10 rounded-md bg-black bg-white p-4 text-lg shadow-md dark:bg-stone-700">
                <Menu.Item>
                  {() => (
                    <div className="border-b-2 border-stone-600 pb-4">
                      <p className="inline-block w-full rounded-md text-left">
                        {`${session.firstname} ${session.lastname}`}
                      </p>
                      <p className="w-full rounded-md text-left text-sm text-stone-600 dark:text-stone-400">
                        {session.email}
                      </p>
                    </div>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <Link
                      href="/profile"
                      className={`${active && "bg-stone-100 dark:bg-stone-600"} mt-2 flex w-full items-center rounded-md p-2 text-left`}
                    >
                      <IconUser stroke={1.5} />
                      <span className="ml-3">{t("profile")}</span>
                    </Link>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <button
                      className={`${active && "bg-stone-100 dark:bg-stone-600"} flex w-full items-center rounded-md p-2 text-left`}
                      onClick={() => signOut()}
                    >
                      <IconLogout stroke={1.5} />
                      <span className="ml-3">{t("logout")}</span>
                    </button>
                  )}
                </Menu.Item>
              </Menu.Items>
            </>
          )}
        </Menu>
      ) : (
        <button onClick={() => signIn("amiv")}>
          <div className="flex">
            <IconLock stroke={1.5} />
            <p className="ml-3">{t("login")}</p>
          </div>
        </button>
      )}
    </>
  );
}
