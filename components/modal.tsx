import { ReactNode } from "react";

import { Dialog } from "@headlessui/react";

import { IconX } from "@tabler/icons-react";

export default function Modal({
  open,
  onClose,
  title,
  description,
  children,
}: {
  open: boolean;
  onClose: () => void;
  title: string;
  description: string;
  children: ReactNode | ReactNode[];
}) {
  return (
    <Dialog open={open} onClose={onClose}>
      <div className="fixed inset-0 bg-black/30 backdrop-blur-sm" />
      <div className="fixed inset-0 flex w-screen items-start justify-center p-6">
        <Dialog.Panel className="relative w-full max-w-[512px] rounded-md bg-white p-6 text-black shadow-md dark:bg-stone-700 dark:text-stone-50">
          <div className="absolute right-6 top-6">
            <button onClick={onClose}>
              <IconX />
            </button>
          </div>
          <Dialog.Title className="text-2xl font-semibold">
            {title}
          </Dialog.Title>
          <Dialog.Description>{description}</Dialog.Description>

          {children}
        </Dialog.Panel>
      </div>
    </Dialog>
  );
}
