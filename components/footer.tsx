interface TitleType {
  de: string;
  en: string;
}

interface ChildItemType {
  title: TitleType;
  href: string;
}

interface ExternalNavItemType {
  title: TitleType;
  childItems: ChildItemType[];
}

async function getAmivStatic() {
  // revalidate data after 1h
  const res = await fetch(
    "https://static.vseth.ethz.ch/assets/vseth-1102-amiv/config.json",
    { next: { revalidate: 3600 } },
  );

  if (!res.ok) {
    throw new Error("Fetching Data from VSETH static failed.");
  }

  return res.json();
}

export default async function Footer({ locale }: { locale: "de" | "en" }) {
  const config = await getAmivStatic();

  return (
    <footer
      style={{ boxShadow: "inset 0 4px 16px 0 rgb(0 0 0 / 0.2)" }}
      className="relative h-fit w-full overflow-x-hidden bg-stone-50 bg-white dark:bg-stone-900"
    >
      <div className="z-0 mx-auto grid max-w-[1200px] grid-cols-1 px-6 py-12 sm:grid-cols-3">
        <div className="col-span-2">
          <div className="grid grid-cols-2 md:grid-cols-4">
            {config.externalNav.map((cat: ExternalNavItemType, i: number) => (
              <div className="my-2" key={i}>
                <p className="mb-2 font-semibold dark:text-stone-200">
                  {cat.title[locale || "en"]}
                </p>
                {cat.childItems.map((item: ChildItemType, j: number) => (
                  <p key={j} className="pb-1">
                    <a
                      className="text-stone-600 hover:underline dark:text-stone-400"
                      target="_blank"
                      href={item.href}
                    >
                      {item.title[locale || "en"]}
                    </a>
                  </p>
                ))}
              </div>
            ))}
          </div>
        </div>
        <div className="mx-auto my-4 flex w-full max-w-56 flex-col gap-8 px-6">
          <a href="https://amiv.ethz.ch" target="_blank">
            <img src="/images/amiv.svg" alt="AMIV Logo" className="w-full" />
          </a>
          <a
            href="https://vseth.ethz.ch"
            target="_blank"
            className="block dark:hidden"
          >
            <img src="/images/vseth.svg" alt="VSETH Logo" className="w-full" />
          </a>
          <a
            href="https://vseth.ethz.ch"
            target="_blank"
            className="hidden dark:block"
          >
            <img
              src="/images/vseth_white.svg"
              alt="VSETH Logo"
              className="w-full"
            />
          </a>
        </div>
      </div>
    </footer>
  );
}
