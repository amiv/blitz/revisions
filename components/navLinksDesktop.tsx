"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";

import Breadcrumbs from "@/components/breadcrumbs";

export default function NavLinksDesktop({
  navItems,
  breadcrumbs,
}: {
  navItems: { label: string; path: string; match: string }[];
  breadcrumbs: { label: string; path: string; translate: boolean }[];
}) {
  const pathnameFull = usePathname();
  const pathname = pathnameFull.slice(4);

  return (
    <div className="hidden h-16 w-full items-end justify-between sm:flex">
      <ul className="flex">
        {navItems
          .filter((i) => i)
          .map((item) => (
            <li
              key={item.path}
              className={`mr-8 block pb-2 ${new RegExp(item.match).test(pathname) && "border-b-2 border-black font-semibold dark:border-stone-200"}`}
            >
              <Link href={item.path}>{item.label}</Link>
            </li>
          ))}
      </ul>

      <Breadcrumbs items={breadcrumbs} />
    </div>
  );
}
