export const dynamic = "force-dynamic";

import Link from "next/link";

import { getServerSession } from "next-auth/next";
import { getToken } from "next-auth/jwt";

import { getI18n, getStaticParams } from "@/locales/server";
import { setStaticParamsLocale } from "next-international/server";

import LanguageMenu from "./languageMenu";
import NavLinks from "./navLinks";
import SessionHandler from "./sessionHandler";
import auth from "@/lib/auth";

export function generateStaticParams() {
  return getStaticParams();
}

export default async function Navbar({ locale }: { locale: string }) {
  setStaticParamsLocale(locale || "en");
  const session = await auth();

  const t = await getI18n();
  //const t = (a) => {};

  const navItems = [
    {
      label: t("home"),
      match: "^(issue/[0-9]+(/article/[0-9]+)?)?$",
      path: "/",
    },
    session &&
      session.isAdmin && {
        label: t("administration"),
        match: "admin",
        path: "/admin",
      },
  ];

  return (
    <nav className="w-full bg-white px-6 pt-6 text-black shadow-md dark:border-b dark:border-stone-600 dark:bg-stone-800 dark:text-stone-200">
      <div className="flex h-8 justify-between sm:h-12">
        <Link href="/" className="inline-flex h-full items-baseline">
          <img
            className="block h-full dark:hidden"
            src="/logo.svg"
            alt="blitz Logo"
          />
          <img
            className="hidden h-full dark:block"
            src="/logo-white.svg"
            alt="blitz Logo"
          />
          <p className="hidden pl-10 text-lg sm:inline">Revisions</p>
        </Link>
        <div className="min-w-content flex items-center gap-4 sm:gap-8">
          <div className="flex h-full flex-col items-end justify-center">
            <LanguageMenu locale={locale} />
          </div>
          <div className="w-content flex flex-col items-end">
            <SessionHandler session={session} />
          </div>
        </div>
      </div>

      <NavLinks navItems={navItems} />
    </nav>
  );
}
