"use client";

import { useRouter } from "next/navigation";

import { useSession } from "next-auth/react";

import { Listbox } from "@headlessui/react";

import { IconSelector } from "@tabler/icons-react";

import { useI18n } from "@/locales/client";

import { isAllowed } from "@/utils/authorization";

import { ArticleType, IssueType } from "@/types/types";

import { gql, useQuery, useMutation } from "@apollo/client";

const getNextIssuesQuery = gql`
  query getNextIssues {
    getNextIssues {
      id
      number
      semester
      name
    }
  }
`;

const moveArticleMutation = gql`
  mutation moveArticle($issueId: Int, $articleId: Int) {
    moveArticle(issueId: $issueId, articleId: $articleId) {
      status
      message
    }
  }
`;

interface ReducedIssueType {
  id: number;
  label: string;
}

export default function EditIssue({
  articleId,
  issueId,
  article,
}: {
  articleId: number;
  issueId: number;
  article: ArticleType | undefined;
}) {
  const { data: issues } = useQuery(getNextIssuesQuery);
  const [moveArticle] = useMutation(moveArticleMutation);
  const t = useI18n();
  const router = useRouter();
  const { data: session } = useSession();

  const issuesData = issues?.getNextIssues.map((i: IssueType) => ({
    id: i.id,
    label: `${i.semester}, ${t("issue")} ${i.number}: ${i.name}`,
  }));

  const current = issuesData?.filter((i: IssueType) => i.id === issueId)[0] || {
    id: 0,
    label: "no issue found",
  };

  const handleChange = async (id: number) => {
    const res = await moveArticle({
      variables: {
        issueId: id,
        articleId: articleId,
      },
    });

    if (res.data.moveArticle.status !== "OK") return;

    router.push(`/issue/${id}/edit/${articleId}`);
  };

  if (!isAllowed("article.move", session, undefined, article?.user.username))
    return <></>;

  return (
    <Listbox value={issueId} onChange={(v) => handleChange(v)}>
      <div className="relative">
        <Listbox.Button className="flex gap-4 rounded-md bg-stone-200 px-3 py-2 font-semibold dark:bg-stone-700">
          {current.label}
          <IconSelector />
        </Listbox.Button>
        <Listbox.Options className="absolute z-50 mt-1 w-full rounded-md bg-stone-200 shadow-md dark:bg-stone-700">
          {issuesData
            ?.toSorted((a: ReducedIssueType, b: ReducedIssueType) =>
              a.label > b.label ? 1 : -1,
            )
            .filter((i: ReducedIssueType) => i.id !== issueId)
            .map((i: ReducedIssueType, index: number) => (
              <Listbox.Option
                key={i.id}
                value={i.id}
                className={`${index > 0 && ""} cursor-pointer rounded-md px-3 py-2 hover:bg-stone-300 dark:hover:bg-stone-600`}
              >
                {i.label}
              </Listbox.Option>
            ))}
        </Listbox.Options>
      </div>
    </Listbox>
  );
}
