import Link from "next/link";

import { IconWriting, IconTextSpellcheck } from "@tabler/icons-react";

import { getI18n } from "@/locales/server";

import StatusSelectionWrapper from "@/components/issue/article/statusSelectionWrapper";

import { formatDateTime } from "@/utils/dates";
import { isAllowedServer } from "@/utils/authorization";

import type { ArticleType, UserType } from "@/types/types";

export default async function Summary({
  article,
  locale,
  user,
}: {
  article: ArticleType;
  locale: string;
  user: UserType;
}) {
  const t = await getI18n();

  return (
    <div className="w-full">
      <div className="flex flex-col items-center justify-between sm:flex-row">
        <div>
          <p>
            {t("createdBy")}{" "}
            <span className="font-semibold">
              {article.user.firstname} {article.user.lastname}
            </span>
          </p>
          <p>
            {t("lastEdited")}{" "}
            <span className="font-semibold">
              {formatDateTime(article.updatedAt, locale)}
            </span>
          </p>
          <p>
            {t("category")}:{" "}
            <span className="font-semibold">
              {article.category === "TOPIC"
                ? article.issue.name
                : t(article.category)}
            </span>
          </p>
        </div>

        <div className="items-right flex flex-col">
          <div className="col-span-full mt-4 flex justify-center gap-2">
            {(await isAllowedServer(
              "article.edit",
              user,
              article.user.username,
            )) &&
              (article.status === "COMPOSITION" ||
                article.status === "LECTORATE") && (
                <Link
                  href={`/issue/${article.issue.id}/edit/${article.id}`}
                  className="overflowX-hidden flex gap-2 whitespace-nowrap rounded-md bg-indigo-600 px-3 py-2 font-semibold text-white shadow-md"
                >
                  <IconWriting />
                  <span>{t("edit")}</span>
                </Link>
              )}
            {(await isAllowedServer(
              "article.lectorate",
              user,
              article.user.username,
            )) && (
              <Link
                href={`/issue/${article.issue.id}/edit/${article.id}`}
                className="overflowX-hidden flex gap-2 whitespace-nowrap rounded-md bg-sky-600 px-3 py-2 font-semibold text-white shadow-md"
              >
                <IconTextSpellcheck />
                <span>{t("lectorate")}</span>
              </Link>
            )}
          </div>
        </div>
      </div>
      <div className="mt-4 flex justify-center">
        <StatusSelectionWrapper article={article} />
      </div>
    </div>
  );
}
