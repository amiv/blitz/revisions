import { useI18n } from "@/locales/client";

import { isAllowed } from "@/utils/authorization";

import { useSession } from "next-auth/react";

import { gql, useQuery, useMutation } from "@apollo/client";

import { ArticleType } from "@/types/types";

const getUserQuery = gql`
  query getUser {
    getUser {
      username
      roles
    }
  }
`;

const updateStatusMutation = gql`
  mutation updateStatus($articleId: Int, $status: Status) {
    updateStatus(articleId: $articleId, status: $status) {
      status
      message
    }
  }
`;

export const statuses = ["COMPOSITION", "LECTORATE", "LAYOUT", "FINISHED"];

export default function StatusSelection({
  status,
  refetch,
  article,
}: {
  status: string;
  refetch: () => void;
  article: ArticleType;
}) {
  const t = useI18n();
  const { data: session } = useSession();
  const author = article?.user.username;

  const { data: user } = useQuery(getUserQuery);
  const [updateStatus] = useMutation(updateStatusMutation);

  const composition =
    session?.isAdmin ||
    isAllowed("article.edit", session, user?.getUser, author);
  const lectorate =
    session?.isAdmin ||
    isAllowed("article.edit", session, user?.getUser, author) ||
    isAllowed("article.lectorate", session, user?.getUser, author);
  const layout =
    session?.isAdmin ||
    isAllowed("article.lectorate", session, user?.getUser, author) ||
    isAllowed("article.layout", session, user?.getUser, author);
  const finished =
    session?.isAdmin ||
    isAllowed("article.layout", session, user?.getUser, author);

  const active = (status: string) =>
    `${getColor(status)} text-white font-semibold shadow-md`;
  const available = () => "bg-stone-200 dark:bg-stone-600 dark:text-white";
  const disabled = () =>
    "bg-stone-100 text-stone-500 cursor-not-allowed dark:bg-stone-700 dark:text-stone-400";

  const getStyle = (key: string, allowed: boolean) => {
    if (key === status) return active(key);
    if (allowed) return available();
    return disabled();
  };

  const getColor = (status: string) => {
    if (status === "COMPOSITION") return "bg-indigo-600";
    if (status === "LECTORATE") return "bg-sky-600";
    if (status === "LAYOUT") return "bg-teal-600";
    return "bg-green-600";
  };

  const handleSwitch = async (status: string) => {
    if (!article) return;

    const res = await updateStatus({
      variables: {
        articleId: article.id,
        status,
      },
    });

    refetch();
  };

  return (
    <div className="flex flex-col sm:flex-row">
      <button
        onClick={() => handleSwitch("COMPOSITION")}
        className={`${getStyle("COMPOSITION", composition)} rounded-b-none rounded-t-md px-3 py-2 sm:rounded-l-md sm:rounded-r-none`}
        disabled={!composition}
      >
        {t("COMPOSITION")}
      </button>
      <button
        onClick={() => handleSwitch("LECTORATE")}
        className={`${getStyle("LECTORATE", lectorate)} border-b border-t border-stone-300 px-3 py-2 sm:border-x sm:border-y-0 dark:border-stone-500`}
        disabled={!lectorate}
      >
        {t("LECTORATE")}
      </button>
      <button
        onClick={() => handleSwitch("LAYOUT")}
        className={`${getStyle("LAYOUT", layout)} border-b border-stone-300 px-3 py-2 sm:border-b-0 sm:border-r dark:border-stone-500`}
        disabled={!layout}
      >
        {t("LAYOUT")}
      </button>
      <button
        onClick={() => handleSwitch("FINISHED")}
        className={`${getStyle("FINISHED", finished)} rounded-b-md rounded-t-none px-3 py-2 sm:rounded-l-none sm:rounded-r-md`}
        disabled={!finished}
      >
        {t("FINISHED")}
      </button>
    </div>
  );
}
