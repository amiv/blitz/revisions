import { useEffect } from "react";

import { getRelativeTime } from "@/utils/dates";

import { IconLoader2, IconDeviceFloppy } from "@tabler/icons-react";

import { useI18n } from "@/locales/client";

export default function Autosave({
  save,
  saving,
  lastSaved,
  locale,
  ready,
}: {
  save: (isAutosave: boolean) => void;
  saving: boolean;
  lastSaved: Date;
  locale: string;
  ready: boolean;
}) {
  const t = useI18n();

  if (!ready) {
    return (
      <div className="flex items-center gap-2">
        <div className="flex gap-2">
          <IconLoader2 className="animate-spin" />
          <span>{t("loadingArticle")}</span>
        </div>
      </div>
    );
  }

  return (
    <div className="flex items-center gap-2">
      {saving ? (
        <div className="flex gap-2">
          <IconLoader2 className="animate-spin" />
          <span>{t("loading")}</span>
        </div>
      ) : (
        <span>
          {t("savedAgo", { diff: getRelativeTime(lastSaved, locale) })}
        </span>
      )}
      <button
        className="overflowX-auto flex items-center gap-2 whitespace-nowrap rounded-md bg-black px-3 py-2 font-semibold text-white shadow-md dark:bg-stone-100 dark:text-black"
        onClick={() => save(false)}
      >
        <IconDeviceFloppy />
        <span>{t("save")}</span>
        <div className="hidden rounded-sm bg-stone-800 px-2 sm:block dark:bg-stone-200">
          <span className="align-middle font-mono text-xs dark:text-black">
            Ctrl + S
          </span>
        </div>
      </button>
    </div>
  );
}
