"use client";

import { SessionProvider } from "next-auth/react";

import StatusSelection from "./statusSelection";

import { ArticleType } from "@/types/types";

import { useQuery, gql } from "@apollo/client";

const getStatusQuery = gql`
  query getStatus($articleId: Int) {
    getStatus(articleId: $articleId)
  }
`;

export default function StatusSelectionWrapper({
  article,
}: {
  article: ArticleType;
}) {
  const { data: status, refetch } = useQuery(getStatusQuery, {
    variables: {
      articleId: article.id,
    },
  });

  console.log(status);

  return (
    <SessionProvider>
      <StatusSelection
        status={status?.getStatus}
        refetch={refetch}
        article={article}
      />
    </SessionProvider>
  );
}
