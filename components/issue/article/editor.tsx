"use client";
// InitializedMDXEditor.tsx
import type { ForwardedRef } from "react";
import {
  headingsPlugin,
  listsPlugin,
  quotePlugin,
  toolbarPlugin,
  imagePlugin,
  diffSourcePlugin,
  UndoRedo,
  BoldItalicUnderlineToggles,
  BlockTypeSelect,
  ListsToggle,
  CodeToggle,
  Separator,
  InsertImage,
  DiffSourceToggleWrapper,
  thematicBreakPlugin,
  markdownShortcutPlugin,
  MDXEditor,
  type MDXEditorMethods,
  type MDXEditorProps,
} from "@mdxeditor/editor";
import "@mdxeditor/editor/style.css";
import "./editor.css";

interface TitleType {
  title: string;
}

// Only import this to the next file
export default function InitializedMDXEditor({
  editorRef,
  title,
  ...props
}: { editorRef: ForwardedRef<MDXEditorMethods> | null } & MDXEditorProps &
  TitleType) {
  async function imageUploadHandler(image: File) {
    const formData = new FormData();
    formData.append("image", image);
    // send the file to your server and return
    // the URL of the uploaded image in the response
    const response = await fetch("/api/upload", {
      method: "POST",
      body: formData,
    });
    const json = (await response.json()) as { url: string };
    return json.url;
  }

  return (
    <MDXEditor
      plugins={[
        // Example Plugin Usage
        headingsPlugin(),
        listsPlugin(),
        quotePlugin(),
        thematicBreakPlugin(),
        markdownShortcutPlugin(),
        imagePlugin({ imageUploadHandler }),
        diffSourcePlugin({
          diffMarkdown: "This feature is being worked on",
          viewMode: "rich-text",
          readOnlyDiff: true,
        }),
        toolbarPlugin({
          toolbarContents: () => (
            <>
              <div className="h-full rounded-md bg-black shadow-md dark:bg-white">
                <h2 className="px-3 text-lg font-bold text-white dark:text-black">
                  {title}
                </h2>
              </div>
              <Separator />
              <UndoRedo />
              <Separator />
              <BoldItalicUnderlineToggles />
              <Separator />
              <BlockTypeSelect />
              <Separator />
              <CodeToggle />
              <Separator />
              <ListsToggle />
              <Separator />
              <InsertImage />
              <Separator />
              <DiffSourceToggleWrapper>
                <></>
              </DiffSourceToggleWrapper>
            </>
          ),
        }),
      ]}
      {...props}
      ref={editorRef}
      contentEditableClassName="prose"
      className="mt-4 shadow-md"
    />
  );
}
