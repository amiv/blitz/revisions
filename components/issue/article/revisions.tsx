"use client";

import { useState } from "react";
import type { Dispatch } from "react";

import { applyPatch } from "diff";

import { IconFileDiff, IconRestore } from "@tabler/icons-react";

import { useI18n } from "@/locales/client";

import { formatDateTime } from "@/utils/dates";

import type { RevisionType } from "@/types/types";

interface HunkType {
  value: string;
  added: boolean;
  removed: boolean;
  count: number;
}

export default function Revisions({
  revisions,
  locale,
  editMode,
  setAbstract,
  setArticle,
}: {
  revisions: RevisionType[] | undefined;
  locale: string;
  editMode: boolean;
  setAbstract?: (value: string) => void;
  setArticle?: (value: string) => void;
}) {
  const t = useI18n();

  const [showDiff, setShowDiff] = useState<boolean>(false);
  const [index, setIndex] = useState<number>(0);

  if (!revisions) return;

  const formatDiff = (diff: string) => {
    const patch = JSON.parse(diff);
    const hunks = patch.map((part: HunkType, i: number) => {
      // green for additions, red for deletions
      const col = part.added ? "green" : part.removed ? "red" : "default";

      return (
        <div
          key={i}
          className={`inline-block whitespace-pre-wrap ${col === "green" ? "rounded-sm bg-green-100 font-semibold text-green-600 dark:bg-green-700 dark:text-white" : col === "red" ? "rounded-sm bg-red-100 font-semibold text-red-600 dark:bg-red-600 dark:text-white" : ""}`}
        >
          {part.value}
        </div>
      );
    });
    return <div className="font-mono">{hunks}</div>;
  };

  const restore = (id: number) => {
    if (!setAbstract || !setArticle) return;

    const revisionsToBeApplied = revisions.filter((r) => r.id <= id);
    const abstractPatches = revisionsToBeApplied.map((r) => r.diffAbstract);
    const articlePatches = revisionsToBeApplied.map((r) => r.diffArticle);

    let abstract = "";
    let article = "";

    abstractPatches.forEach((p) => {
      const res = applyPatch(abstract, p);
      if (res === false) return;
      abstract = res;
    });
    articlePatches.forEach((p) => {
      const res = applyPatch(article, p);
      if (res === false) return;
      article = res;
    });

    setAbstract(abstract);
    setArticle(article);
  };

  const getColor = (status: string) => {
    if (status === "COMPOSITION") return "bg-indigo-600";
    if (status === "LECTORATE") return "bg-sky-600";
    if (status === "LAYOUT") return "bg-teal-600";
    return "bg-green-600";
  };

  return (
    <div className="mt-8 rounded-md bg-white p-4 shadow-md dark:bg-stone-700">
      <h2 className="text-2xl font-black">Revisions</h2>

      <div>
        {revisions.map((r, i) => (
          <div
            className={`grid grid-cols-1 items-center gap-4 py-2 sm:grid-cols-3 ${i !== 0 && "border-t border-stone-600"}`}
            key={i}
          >
            <div>
              <p>{`${r.user.firstname} ${r.user.lastname}`}</p>
              <p className="text-stone-500 dark:text-stone-400">
                {formatDateTime(r.timestamp, locale)}
              </p>
              <div
                className={`${getColor(r.status)} mt-1 w-fit rounded-full px-2 font-semibold text-white shadow-md`}
              >
                <span className="text-sm">{t(r.status)}</span>
              </div>
            </div>
            {r.isAutosave ? (
              <div>
                <div className="w-fit rounded-full bg-black px-2 font-semibold text-white shadow-md sm:mx-auto dark:bg-white dark:text-black">
                  <span className="text-sm">Autosave</span>
                </div>
              </div>
            ) : (
              <div className="hidden sm:block" />
            )}
            <div className="flex items-center gap-2 sm:justify-end">
              <button
                onClick={() => {
                  setIndex(r.id);
                  setShowDiff(showDiff && index === r.id ? false : true);
                }}
                className="flex items-center gap-2 rounded-md border border-black bg-white px-3 py-2 font-semibold text-black shadow-md dark:bg-black dark:text-white"
              >
                <IconFileDiff />
                <span>
                  {showDiff && index === r.id ? t("hideDiff") : t("showDiff")}
                </span>
              </button>
              {editMode && (
                <button
                  onClick={() => restore(r.id)}
                  className="flex items-center gap-2 rounded-md bg-black px-3 py-2 font-semibold text-white shadow-md"
                >
                  <IconRestore />
                  <span>{t("restore")}</span>
                </button>
              )}
            </div>
            {showDiff && index === r.id && (
              <div className="col-span-full">
                <div className="mt-4 overflow-x-auto rounded-md p-4 shadow-md dark:bg-stone-600">
                  <h3 className="mb-2 font-black">{t("abstract")}</h3>
                  {formatDiff(r.coAbstract)}
                </div>
                <div className="my-4 overflow-x-auto rounded-md p-4 shadow-md dark:bg-stone-600">
                  <h3 className="mb-2 font-black">{t("article")}</h3>
                  {formatDiff(r.coArticle)}
                </div>
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
}
