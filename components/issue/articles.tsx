import Link from "next/link";

import {
  IconArrowRight,
  IconTextSpellcheck,
  IconWriting,
} from "@tabler/icons-react";

import { getI18n } from "@/locales/server";

import { isAllowed } from "@/utils/authorization";

import { ArticleType, UserType } from "@/types/types";

export default async function Articles({
  articles,
  issueId,
  user,
}: {
  articles: ArticleType[];
  issueId: string;
  user: UserType | undefined;
}) {
  const t = await getI18n();

  const getColor = (status: string) => {
    if (status === "COMPOSITION") return "bg-indigo-600";
    if (status === "LECTORATE") return "bg-sky-600";
    if (status === "LAYOUT") return "bg-teal-600";
    return "bg-green-600";
  };

  return (
    <div className="w-full rounded-md bg-white px-6 shadow-md dark:bg-stone-700">
      {articles.map((article, i) => (
        <div
          className={`${i > 0 && "border-t border-stone-600"} flex w-full flex-col justify-between gap-4 py-6 sm:flex-row`}
          key={i}
        >
          <div>
            <p className="font-semibold">{article.title || t("noTitle")}</p>
            <p>
              {article.author ? `${t("by")} ${article.author}` : t("noAuthor")}
            </p>
            <div
              className={`${getColor(article.status)} mt-1 w-fit rounded-full px-2 font-semibold text-white shadow-md`}
            >
              <span className="text-sm">{t(article.status)}</span>
            </div>
          </div>
          {isAllowed("article.view", undefined, user, undefined) && (
            <div className="flex flex-wrap-reverse items-center justify-start gap-2 sm:justify-end sm:justify-center">
              {isAllowed(
                "article.edit",
                undefined,
                user,
                article.user?.username,
              ) &&
                (article.status === "LECTORATE" ||
                  article.status === "COMPOSITION") && (
                  <Link
                    href={`/issue/${issueId}/edit/${article.id}`}
                    className="flex w-fit gap-2 whitespace-nowrap rounded-md bg-indigo-600 px-3 py-2 font-semibold text-white shadow-md hover:bg-indigo-500"
                  >
                    <IconWriting />
                    {t("edit")}
                  </Link>
                )}
              {isAllowed("article.lectorate", undefined, user, undefined) &&
                article.status === "LECTORATE" && (
                  <Link
                    href={`/issue/${issueId}/edit/${article.id}`}
                    className="flex w-fit gap-2 whitespace-nowrap rounded-md bg-sky-600 px-3 py-2 font-semibold text-white shadow-md hover:bg-sky-500"
                  >
                    <IconTextSpellcheck />
                    {t("lectorate")}
                  </Link>
                )}
              <Link
                href={`/issue/${issueId}/article/${article.id}`}
                className="flex w-fit gap-2 rounded-md bg-black px-3 py-2 font-semibold text-white shadow-md hover:bg-stone-800 dark:bg-stone-100 dark:text-black dark:hover:bg-stone-200"
              >
                {t("view")}
                <IconArrowRight />
              </Link>
            </div>
          )}
        </div>
      ))}
    </div>
  );
}
