"use client";

import { useRouter } from "next/navigation";

import { useState } from "react";

import { useI18n } from "@/locales/client";

import { IconPencilPlus, IconX } from "@tabler/icons-react";

import Alert from "@/components/alert";

import { gql, useMutation } from "@apollo/client";

const addArticleMutation = gql`
  mutation addArticle($issueId: Int) {
    addArticle(issueId: $issueId) {
      status
      message
    }
  }
`;

export default function AddArticleButton({ issueId }: { issueId: number }) {
  const [addArticle] = useMutation(addArticleMutation);
  const router = useRouter();
  const t = useI18n();

  const [showError, setShowError] = useState<boolean>(false);
  const [error, setError] = useState<string>("");
  const [errorTitle, setErrorTitle] = useState<string>("");

  const handleSubmit = async () => {
    const { data } = await addArticle({
      variables: {
        issueId,
      },
    });

    if (data.addArticle.status !== "OK") {
      setErrorTitle(data.addArticle.status);
      setError(data.addArticle.message);
      setShowError(true);
      return;
    }

    setShowError(false);
    router.push(`/issue/${issueId}/edit/${data.addArticle.message}`);
  };

  return (
    <>
      <button
        onClick={handleSubmit}
        className="mx-auto mt-8 flex gap-2 rounded-md bg-black px-3 py-2 font-semibold text-white shadow-md hover:bg-stone-800 dark:bg-stone-100 dark:text-black dark:hover:bg-stone-200"
      >
        <IconPencilPlus />
        <span>{t("addArticle")}</span>
      </button>

      {showError && (
        <Alert
          title={errorTitle}
          icon={<IconX />}
          error={true}
          className="mt-4"
        >
          {error}
        </Alert>
      )}
    </>
  );
}
