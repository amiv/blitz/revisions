"use client";

import { useState } from "react";

import { useSession } from "next-auth/react";

import { IconPlus, IconRocket, IconX } from "@tabler/icons-react";
import { useI18n, useCurrentLocale } from "@/locales/client";

import Modal from "@/components/modal";
import DatePicker from "@/components/datePicker";
import Alert from "@/components/alert";

import type { Value } from "@/types/types";

import { gql, useMutation } from "@apollo/client";

const updateUserMutation = gql`
  mutation updateUser($username: String, $roles: String) {
    updateUser(username: $username, roles: $roles) {
      status
      message
    }
  }
`;

export default function AddIssue({ refetch }: { refetch: () => void }) {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [username, setUsername] = useState<string>("");
  const [roles, setRoles] = useState<boolean[]>([false, false, false]);
  const [errors, setErrors] = useState<string[]>([]);
  const [serverError, setServerError] = useState<string>("");

  const [updateUser] = useMutation(updateUserMutation);

  const t = useI18n();
  const { data: session } = useSession();

  const possibleRoles = ["AUTHOR", "LAYOUT", "LECTOR"] as const;

  const displayValidation = (i: number) => {
    if (errors.length !== 1) return <></>;

    const msg = errors[i];
    return msg ? (
      <p className="text-sm text-red-600 dark:text-red-400">{msg}</p>
    ) : (
      <></>
    );
  };

  const validate = () => {
    let status = true;
    let err = [];

    if (username === "") {
      err.push(t("errUsername"));
      status = false;
    } else err.push("");

    setErrors(err);
    return status;
  };

  const handleSubmit = async () => {
    if (!validate()) return;

    const res = await updateUser({
      variables: {
        username,
        roles: possibleRoles.filter((r, i) => roles[i]).join(","),
      },
    });

    if (res.data.updateUser.status === "OK") {
      refetch();
      setUsername("");
      setRoles([false, false, false]);
      setIsOpen(false);
      setServerError("");
    } else {
      setServerError(res.data.updateUser.message);
    }
  };

  return (
    <>
      <button
        onClick={() => setIsOpen(true)}
        className="mx-auto flex gap-2 rounded-md bg-black px-3 py-2 font-semibold text-white shadow-md hover:bg-stone-800 dark:bg-stone-100 dark:text-black dark:hover:bg-stone-200"
      >
        <IconPlus />
        <span>{t("addUser")}</span>
      </button>
      <Modal
        open={isOpen}
        onClose={() => setIsOpen(false)}
        title={t("addUser")}
        description={t("addUserDescription")}
      >
        <div className="mt-4">
          <div>
            <label>
              <p className="text-sm text-stone-600 dark:text-stone-400">
                {t("username")}
              </p>
              <input
                type="text"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                placeholder={t("usernamePlaceholder")}
                className="w-full border-b border-stone-600 bg-white p-1 dark:border-stone-400 dark:bg-stone-700"
              />
              {displayValidation(0)}
            </label>
          </div>
          <div className="mt-2 grid grid-cols-1 md:grid-cols-3">
            {possibleRoles.map((role, i) => (
              <div key={i}>
                <label className="flex">
                  <input
                    type="checkbox"
                    checked={roles[i]}
                    onChange={() => {
                      setRoles(
                        roles.map((role, j) => (i === j ? !role : role)),
                      );
                    }}
                    value=""
                    className="w-4 accent-black dark:accent-stone-100"
                  />
                  <p className="ml-2 text-sm text-stone-600 dark:text-stone-400">
                    {t(role)}
                  </p>
                </label>
              </div>
            ))}
          </div>
          {serverError && (
            <div className="col-span-full mt-4">
              <Alert
                title={t("serverError")}
                icon={<IconX stroke={1.5} />}
                error={true}
              >
                {serverError}
              </Alert>
            </div>
          )}
          <div className="col-span-full mt-4">
            <button
              onClick={handleSubmit}
              className="mx-auto flex gap-2 rounded-md bg-black px-3 py-2 font-semibold text-white shadow-md hover:bg-stone-800 dark:bg-white dark:text-black hover:dark:bg-stone-200"
            >
              <IconRocket stroke={1.5} />
              <span>{t("addUser")}</span>
            </button>
          </div>
        </div>
      </Modal>
    </>
  );
}
