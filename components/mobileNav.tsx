"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";

import { useState } from "react";

import { IconMenu2, IconX } from "@tabler/icons-react";

// import { useI18n } from "@/locales/client";

import LanguageMenu from "@/components/languageMenu";
import Breadcrumbs from "@/components/breadcrumbs";

export default function MobileNav({
  navItems,
  breadcrumbs,
}: {
  navItems: { label: string; path: string; match: string }[];
  breadcrumbs: { label: string; path: string; translate: boolean }[];
}) {
  const pathnameFull = usePathname();
  const pathname = pathnameFull.slice(4);

  const [mobileNavOpen, setMobileNavOpen] = useState<boolean>(false);

  return (
    <div className="block h-fit w-full sm:hidden">
      <div className="flex h-16 items-center justify-between">
        <div className="flex items-center">
          <button
            className="flex"
            onClick={() => setMobileNavOpen(!mobileNavOpen)}
          >
            {mobileNavOpen ? <IconX /> : <IconMenu2 />}
          </button>
          <span className="ml-4 text-lg">Revisions</span>
        </div>
      </div>

      <div
        className={`overflow-y-hidden ${!mobileNavOpen ? "hidden" : "block"}`}
      >
        <div className="w-full pb-6">
          <Breadcrumbs items={breadcrumbs} />
          <div className="mt-2 rounded-md bg-stone-100 px-6 py-6 dark:bg-stone-700">
            <ul>
              {navItems
                .filter((i) => i)
                .map((item) => (
                  <li
                    key={item.path}
                    className={`block px-3 py-2 text-xl ${new RegExp(item.match).test(pathname) && "rounded-md bg-black font-semibold text-white shadow-md dark:border-stone-200 dark:bg-stone-100 dark:text-black"}`}
                  >
                    <Link href={item.path}>{item.label}</Link>
                  </li>
                ))}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
