-- AlterTable
ALTER TABLE `Article` ADD COLUMN `isAutosave` BOOLEAN NOT NULL DEFAULT true;

-- AlterTable
ALTER TABLE `Revision` ADD COLUMN `isAutosave` BOOLEAN NOT NULL DEFAULT true;
