-- DropIndex
DROP INDEX `User_amivId_key` ON `User`;

-- DropIndex
DROP INDEX `User_email_key` ON `User`;

-- AlterTable
ALTER TABLE `User` MODIFY `email` VARCHAR(191) NULL DEFAULT '',
    MODIFY `firstname` VARCHAR(191) NULL DEFAULT '',
    MODIFY `lastname` VARCHAR(191) NULL DEFAULT '',
    MODIFY `amivId` VARCHAR(191) NOT NULL DEFAULT '';
