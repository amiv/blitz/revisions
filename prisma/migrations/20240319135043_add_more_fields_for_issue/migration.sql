/*
  Warnings:

  - Added the required column `deadline` to the `Issue` table without a default value. This is not possible if the table is not empty.
  - Added the required column `publicationDate` to the `Issue` table without a default value. This is not possible if the table is not empty.
  - Added the required column `semester` to the `Issue` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `Issue` ADD COLUMN `deadline` DATETIME(3) NOT NULL,
    ADD COLUMN `publicationDate` DATETIME(3) NOT NULL,
    ADD COLUMN `semester` VARCHAR(191) NOT NULL;
