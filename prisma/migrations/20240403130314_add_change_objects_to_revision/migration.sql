/*
  Warnings:

  - Added the required column `coAbstract` to the `Revision` table without a default value. This is not possible if the table is not empty.
  - Added the required column `coArticle` to the `Revision` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `Revision` ADD COLUMN `coAbstract` TEXT NOT NULL,
    ADD COLUMN `coArticle` TEXT NOT NULL;
