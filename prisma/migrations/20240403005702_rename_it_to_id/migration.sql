/*
  Warnings:

  - The primary key for the `Revision` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `it` on the `Revision` table. All the data in the column will be lost.
  - Added the required column `id` to the `Revision` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `Revision` DROP PRIMARY KEY,
    DROP COLUMN `it`,
    ADD COLUMN `id` INTEGER NOT NULL AUTO_INCREMENT,
    ADD PRIMARY KEY (`id`);
