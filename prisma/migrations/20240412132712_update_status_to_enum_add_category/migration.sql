/*
  Warnings:

  - You are about to alter the column `status` on the `Article` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Enum(EnumId(2))`.
  - You are about to alter the column `status` on the `Revision` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Enum(EnumId(2))`.

*/
-- AlterTable
ALTER TABLE `Article` ADD COLUMN `category` ENUM('BLITZ', 'AMIV', 'TOPIC', 'STUDIES') NOT NULL DEFAULT 'TOPIC',
    MODIFY `status` ENUM('COMPOSITION', 'LECTORATE', 'LAYOUT', 'FINISHED') NOT NULL DEFAULT 'COMPOSITION';

-- AlterTable
ALTER TABLE `Revision` MODIFY `status` ENUM('COMPOSITION', 'LECTORATE', 'LAYOUT', 'FINISHED') NOT NULL DEFAULT 'COMPOSITION';
