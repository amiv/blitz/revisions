import type { Metadata, ResolvingMetadata } from "next";

import { isAllowedServer } from "@/utils/authorization";
import Unauthorized from "@/components/unauthorized";

type Props = {
  params: { locale: "de" | "en" };
};

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata,
): Promise<Metadata> {
  const locale = params.locale;

  return locale === "de"
    ? {
        title: "Admin | Revisions - Redaktionssystem des blitz",
        description:
          "Redaktionssystem des blitz, der Fachzeitschrift des AMIV an der ETH",
      }
    : {
        title: "Admin | Revisions - Editorial System of blitz",
        description:
          "Editorial System of blitz, the student magazine of AMIV at ETH",
      };
}

export default async function AdminLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  if (!(await isAllowedServer("users.view", undefined, undefined)))
    return <Unauthorized target="users.view" />;
  return <>{children}</>;
}
