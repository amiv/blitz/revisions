"use client";

import { SessionProvider } from "next-auth/react";

import { useI18n } from "@/locales/client";

import Title from "@/components/title";
import Loading from "@/components/loading";
import AddUserModal from "@/components/admin/addUserModal";

import type { UserType } from "@/types/types";

import { gql, useQuery, useMutation } from "@apollo/client";

const getUsersQuery = gql`
  query getUsers {
    getUsers {
      firstname
      lastname
      username
      email
      roles
    }
  }
`;

const updateUserMutation = gql`
  mutation updateUser($username: String, $roles: String) {
    updateUser(username: $username, roles: $roles) {
      status
      message
    }
  }
`;

export default function Admin({
  params: { locale },
}: {
  params: { locale: string };
}) {
  const { data, refetch, loading } = useQuery(getUsersQuery);
  const [updateUser] = useMutation(updateUserMutation);
  const t = useI18n();

  const possibleRoles = ["AUTHOR", "LAYOUT", "LECTOR"] as const;

  const setRoles = async (roles: string, username: string) => {
    await updateUser({
      variables: {
        username,
        roles,
      },
    });
    refetch();
  };

  return (
    <div>
      <Title>
        <h1 className="text-6xl font-black">Administration</h1>
      </Title>

      <div className="mx-auto w-full max-w-[1200px] px-6 py-12">
        <SessionProvider>
          <AddUserModal refetch={refetch} />
        </SessionProvider>

        <div className="mt-8 overflow-x-auto">
          {!loading && data ? (
            <table className="w-full min-w-48">
              <thead className="rounded-md bg-stone-100 text-left font-semibold shadow-md">
                <tr>
                  <th className="p-4">{t("lastname")}</th>
                  <th className="p-4">{t("firstname")}</th>
                  <th className="p-4">{t("username")}</th>
                  <th className="p-4">{t("email")}</th>
                  {possibleRoles.map((r) => (
                    <th className="p-4" key={r}>
                      {t(r)}
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody>
                {data.getUsers.map((user: UserType, i: number) => {
                  const roles = user.roles.split(",");
                  const rolesBool = possibleRoles.map((r) => roles.includes(r));

                  return (
                    <tr key={i}>
                      <td className="p-4 align-top">{user.lastname}</td>
                      <td className="p-4 align-top">{user.firstname}</td>
                      <td className="p-4 align-top">{user.username}</td>
                      <td className="p-4 align-top">{user.email}</td>
                      {possibleRoles.map((role, j) => (
                        <td className="p-4 align-top" key={j}>
                          <input
                            type="checkbox"
                            checked={rolesBool[j]}
                            onChange={() => {
                              const newRoles = rolesBool.map((r, k) =>
                                j === k ? !r : r,
                              );
                              setRoles(
                                possibleRoles
                                  .filter((r, k) => newRoles[k])
                                  .join(","),
                                user.username,
                              );
                            }}
                            value=""
                            className="w-4 accent-black dark:accent-stone-100"
                          />
                        </td>
                      ))}
                    </tr>
                  );
                })}
              </tbody>
            </table>
          ) : (
            <Loading />
          )}
        </div>
      </div>
    </div>
  );
}
