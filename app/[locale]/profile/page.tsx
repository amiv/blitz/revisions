import type { Metadata, ResolvingMetadata } from "next";

import { IconCheck, IconX } from "@tabler/icons-react";

import { getI18n, getStaticParams } from "@/locales/server";
import { setStaticParamsLocale } from "next-international/server";

import auth from "@/lib/auth";

import Title from "@/components/title";

export function generateStaticParams() {
  return getStaticParams();
}

type Props = {
  params: { locale: "de" | "en" };
};

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata,
): Promise<Metadata> {
  const locale = params.locale;

  return locale === "de"
    ? {
        title: "Profil | Revisions - Redaktionssystem des blitz",
        description:
          "Redaktionssystem des blitz, der Fachzeitschrift des AMIV an der ETH",
      }
    : {
        title: "Profile | Revisions - Editorial System of blitz",
        description:
          "Editorial System of blitz, the student magazine of AMIV at ETH",
      };
}

export default async function Profile({
  params: { locale },
}: {
  params: { locale: string };
}) {
  setStaticParamsLocale(locale || "en");
  const t = await getI18n();

  const session = await auth();

  const data = [
    {
      name: t("admin"),
      status: session?.isAdmin,
      description: t("adminDescription"),
    },
    {
      name: t("member"),
      status: session?.isMember,
      description: t("memberDescription"),
    },
  ];

  return (
    <div>
      <Title>
        <h1 className="text-5xl font-black sm:text-7xl">{t("profile")}</h1>
      </Title>

      <div className="mx-auto w-full max-w-[1200px] px-6 py-12">
        <h2 className="text-4xl font-black">{t("permissions")}</h2>

        <table className="mt-8">
          <thead className="rounded-md bg-stone-100 text-left font-semibold shadow-sm">
            <tr>
              <th className="p-4">{t("role")}</th>
              <th className="p-4">{t("status")}</th>
              <th className="p-4">{t("description")}</th>
            </tr>
          </thead>
          <tbody>
            {data.map((entry, i) => (
              <tr key={i}>
                <td className="p-4 align-top">{entry.name}</td>
                <td className="p-4 align-top">
                  {entry.status ? <IconCheck /> : <IconX />}
                </td>
                <td className="p-4 align-top">{entry.description}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
