export const dynamic = "force-dynamic";

import { getI18n, getStaticParams } from "@/locales/server";
import { setStaticParamsLocale } from "next-international/server";

import Title from "@/components/title";
import NextIssueCard from "@/components/main/nextIssueCard";
import Articles from "@/components/issue/articles";
import AddArticleButton from "@/components/issue/addArticleButton";

import { isAllowedServer } from "@/utils/authorization";

import { gql } from "@apollo/client";
import { getClient } from "@/graphql/ApolloClient";

import auth from "@/lib/auth";

const getIssueByIdQuery = gql`
  query getIssueById($id: Int) {
    getIssueById(id: $id) {
      id
      name
      articles {
        id
        title
        author
        status
        user {
          username
        }
      }
      publicationDate
      deadline
      semester
      number
    }
  }
`;

const getUserQuery = gql`
  query getUser {
    getUser {
      username
      roles
    }
  }
`;

export function generateStaticParams() {
  return getStaticParams();
}

export default async function Issue({
  params: { locale, id },
}: {
  params: { locale: string; id: string };
}) {
  setStaticParamsLocale(locale || "en");

  const t = await getI18n();

  const { data } = await getClient().query({
    query: getIssueByIdQuery,
    variables: { id: Number(id) },
  });
  const { data: user } = await getClient().query({
    query: getUserQuery,
  });

  return (
    <div>
      <Title>
        <NextIssueCard
          t={t}
          issue={data.getIssueById}
          locale={locale}
          showLogo={true}
          showNextIssue={false}
          showArticles={false}
          showViewButton={false}
        />
        {(await isAllowedServer(
          "article.create",
          user?.getUser,
          undefined,
        )) && <AddArticleButton issueId={Number(id)} />}
      </Title>
      <div className="mx-auto w-full max-w-[1200px] px-6 py-12 text-black dark:text-stone-100">
        <Articles
          articles={data.getIssueById.articles}
          issueId={id}
          user={user?.getUser}
        />
      </div>
    </div>
  );
}
