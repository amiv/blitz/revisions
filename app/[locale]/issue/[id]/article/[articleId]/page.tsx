export const dynamic = "force-dynamic";

import type { Metadata, ResolvingMetadata } from "next";
import Link from "next/link";

import type { ReactNode } from "react";

import Markdown from "react-markdown";
import rehypeRaw from "rehype-raw";
import remarkGfm from "remark-gfm";

import { getI18n, getStaticParams } from "@/locales/server";
import { setStaticParamsLocale } from "next-international/server";

import auth from "@/lib/auth";
import { isAllowedServer } from "@/utils/authorization";

import Separator from "@/components/separator";
import Title from "@/components/title";
import Revisions from "@/components/issue/article/revisions";
import Summary from "@/components/issue/article/summary";
import Unauthorized from "@/components/unauthorized";
import StatusSelectionWrapper from "@/components/issue/article/statusSelectionWrapper";

import type { Element } from "hast";

type Components = Partial<{
  [TagName in keyof JSX.IntrinsicElements]:  // Class component:
    | (new (props: JSX.IntrinsicElements[TagName]) => JSX.ElementClass)
    // Function component:
    | ((
        props: JSX.IntrinsicElements[TagName],
      ) => JSX.Element | string | null | undefined)
    // Tag name:
    | keyof JSX.IntrinsicElements;
}>;

import { gql } from "@apollo/client";
import { getClient } from "@/graphql/ApolloClient";

const getArticleQuery = gql`
  query getArticle($id: Int) {
    getArticle(id: $id) {
      id
      title
      subtitle
      author
      email
      abstract
      article
      updatedAt
      category
      status
      issue {
        id
        name
        number
        semester
      }
      user {
        username
        firstname
        lastname
      }
    }
  }
`;

const getRevisionsQuery = gql`
  query getRevisions($articleId: Int) {
    getRevisions(articleId: $articleId) {
      id
      timestamp
      user {
        firstname
        lastname
      }
      coAbstract
      coArticle
      diffArticle
      diffAbstract
      isAutosave
      status
    }
  }
`;

const getUserQuery = gql`
  query getUser {
    getUser {
      username
      roles
    }
  }
`;

type Props = {
  params: { locale: "de" | "en" };
};

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata,
): Promise<Metadata> {
  const locale = params.locale;

  return locale === "de"
    ? {
        title: "Artikel Lesen | Revisions - Redaktionssystem des blitz",
        description:
          "Redaktionssystem des blitz, der Fachzeitschrift des AMIV an der ETH",
      }
    : {
        title: "Read Article | Revisions - Editorial System of blitz",
        description:
          "Editorial System of blitz, the student magazine of AMIV at ETH",
      };
}

export function generateStaticParams() {
  return getStaticParams();
}

export default async function ViewArticle({
  params: { locale, id, articleId },
}: {
  params: { locale: string; id: string; articleId: string };
}) {
  setStaticParamsLocale(locale || "en");

  const t = await getI18n();
  const session = await auth();

  const { data } = await getClient().query({
    query: getArticleQuery,
    variables: { id: Number(articleId) },
  });
  const { data: user } = await getClient().query({
    query: getUserQuery,
  });
  const { data: revisions } = await getClient().query({
    query: getRevisionsQuery,
    variables: { articleId: Number(articleId) },
  });

  if (!(await isAllowedServer("article.view", user?.getUser, undefined)))
    return <Unauthorized target="article.view" />;

  const components = {
    h1: ({ children }: { children: ReactNode }) => (
      <h1 className="my-4 text-3xl font-black">{children}</h1>
    ),
    h2: ({ children }: { children: ReactNode }) => (
      <h2 className="my-2 text-2xl font-black">{children}</h2>
    ),
    h3: ({ children }: { children: ReactNode }) => (
      <h3 className="text-xl font-black">{children}</h3>
    ),
    h4: ({ children }: { children: ReactNode }) => (
      <h4 className="font-black">{children}</h4>
    ),
    h5: ({ children }: { children: ReactNode }) => (
      <h5 className="font-bold">{children}</h5>
    ),
    h6: ({ children }: { children: ReactNode }) => (
      <h6 className="font-semibold">{children}</h6>
    ),
    ul: ({ children }: { children: ReactNode }) => (
      <ul className="ml-4 list-inside list-disc">{children}</ul>
    ),
    ol: ({ children }: { children: ReactNode }) => (
      <ol className="ml-4 list-inside list-decimal">{children}</ol>
    ),
    code: ({ children }: { children: ReactNode }) => (
      <code className="bg-stone-300 dark:bg-stone-600">{children}</code>
    ),
    p: ({ children }: { children: ReactNode }) => {
      // @ts-ignore
      if (typeof children === "object" && children?.type?.name === "img") {
        // @ts-ignore
        const { src, alt, title } = children.props;
        return (
          <figure className="flex flex-col items-center justify-center gap-4 sm:flex-row">
            <img
              className="w-full max-w-[450px] rounded-md shadow-md"
              src={src}
              alt={alt}
              title={title}
            />
            <figcaption className="w-full max-w-[450px] text-center sm:text-left">
              <p className="hidden text-xl font-black not-italic sm:inline-block">
                {t("figure")}
              </p>
              <p className="italic">
                <span className="mr-2 inline-block font-black not-italic sm:hidden">
                  {t("figure")}
                </span>
                {title}
              </p>
            </figcaption>
          </figure>
        );
      }
      return <p className="my-2 leading-relaxed">{children}</p>;
    },
    img: ({ src, alt, title }: { src: string; alt: string; title: string }) => (
      <figure className="flex flex-col items-center justify-center gap-4 sm:flex-row">
        <img
          className="w-full max-w-[450px] rounded-md shadow-md"
          src={src}
          alt={alt}
          title={title}
        />
        <figcaption className="w-full max-w-[450px] text-center sm:text-left">
          <p className="hidden text-xl font-black not-italic sm:inline-block">
            {t("figure")}
          </p>
          <p className="italic">
            <span className="mr-2 inline-block font-black not-italic sm:hidden">
              {t("figure")}
            </span>
            {title}
          </p>
        </figcaption>
      </figure>
    ),
  } as Partial<Components>;

  return (
    <div>
      <Title>
        <div className="grid grid-cols-1 gap-2 sm:grid-cols-2">
          {data.getArticle.title && (
            <div className="overflowX-auto col-span-full">
              <p className="w-full bg-transparent py-2 text-left text-4xl font-black sm:text-center sm:text-6xl dark:border-stone-400">
                {data.getArticle.title}
              </p>
            </div>
          )}
          {data.getArticle.subtitle && (
            <div className="overflowX-auto col-span-full mb-8">
              <p className="w-full bg-transparent py-1 text-left text-2xl font-bold text-stone-500 sm:text-center sm:text-4xl dark:border-stone-400 dark:text-stone-400">
                {data.getArticle.subtitle}
              </p>
            </div>
          )}
          {data.getArticle.author && (
            <div>
              <p className="w-full bg-transparent text-left text-lg font-semibold sm:text-left sm:text-2xl dark:border-stone-400">
                {data.getArticle.author}
              </p>
            </div>
          )}
          {data.getArticle.email && (
            <div>
              <p className="w-full bg-transparent text-left text-lg font-semibold sm:text-right sm:text-2xl dark:border-stone-400">
                {data.getArticle.email}
              </p>
            </div>
          )}
        </div>
      </Title>
      <div className="mx-auto w-full max-w-[1200px] px-6 py-12 text-black dark:text-stone-100">
        <Summary
          article={data.getArticle}
          locale={locale}
          user={user.getUser}
        />
        <Separator />
        <Markdown
          /*
          // @ts-ignore */
          components={components}
          className="mb-4 italic"
          rehypePlugins={[rehypeRaw, remarkGfm]}
        >
          {data.getArticle.abstract}
        </Markdown>
        {/*
          // @ts-ignore */}
        <Markdown
          components={components}
          rehypePlugins={[rehypeRaw, remarkGfm]}
        >
          {data.getArticle.article}
        </Markdown>

        <Revisions
          revisions={revisions?.getRevisions}
          locale={locale}
          editMode={false}
        />
      </div>
    </div>
  );
}
