import type { Metadata, ResolvingMetadata } from "next";

import Unauthorized from "@/components/unauthorized";

import { isAllowedServer } from "@/utils/authorization";

import { gql } from "@apollo/client";
import { getClient } from "@/graphql/ApolloClient";

const getArticleQuery = gql`
  query getArticle($id: Int) {
    getArticle(id: $id) {
      user {
        username
      }
    }
  }
`;

const getUserQuery = gql`
  query getUser {
    getUser {
      username
      roles
    }
  }
`;

type Props = {
  params: { locale: "de" | "en" };
};

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata,
): Promise<Metadata> {
  const locale = params.locale;

  return locale === "de"
    ? {
        title: "Artikel Bearbeiten | Revisions - Redaktionssystem des blitz",
        description:
          "Redaktionssystem des blitz, der Fachzeitschrift des AMIV an der ETH",
      }
    : {
        title: "Edit Article | Revisions - Editorial System of blitz",
        description:
          "Editorial System of blitz, the student magazine of AMIV at ETH",
      };
}

export default async function IssueEditArticle({
  children,
  params: { articleId },
}: Readonly<{
  children: React.ReactNode;
  params: {
    articleId: string;
  };
}>) {
  const { data } = await getClient().query({
    query: getArticleQuery,
    variables: { id: Number(articleId) },
  });
  const { data: user } = await getClient().query({
    query: getUserQuery,
  });
  if (
    !(await isAllowedServer(
      "article.edit",
      user.getUser,
      data.getArticle?.user.username,
    ))
  )
    return <Unauthorized target="article.edit" />;
  return <>{children}</>;
}
