"use client";

import Link from "next/link";

import { useState, useEffect, useRef } from "react";

import { SessionProvider } from "next-auth/react";

import { IconX } from "@tabler/icons-react";

import { useI18n } from "@/locales/client";

import Separator from "@/components/separator";
import Title from "@/components/title";
import Autosave from "@/components/issue/article/autosave";
import Alert from "@/components/alert";
import Revisions from "@/components/issue/article/revisions";
import StatusSelection from "@/components/issue/article/statusSelection";
import EditIssue from "@/components/issue/article/editIssue";

import { gql, useQuery, useMutation } from "@apollo/client";

const getArticleQuery = gql`
  query getArticle($id: Int) {
    getArticle(id: $id) {
      id
      title
      subtitle
      author
      email
      abstract
      article
      updatedAt
      status
      issue {
        id
        name
        number
        semester
      }
      user {
        firstname
        lastname
        username
      }
    }
  }
`;

const getRevisionsQuery = gql`
  query getRevisions($articleId: Int) {
    getRevisions(articleId: $articleId) {
      id
      timestamp
      user {
        firstname
        lastname
      }
      coAbstract
      coArticle
      diffAbstract
      diffArticle
      isAutosave
      status
    }
  }
`;

const updateArticleMutation = gql`
  mutation updateArticle(
    $id: Int
    $title: String
    $subtitle: String
    $author: String
    $email: String
    $abstract: String
    $article: String
    $isAutosave: Boolean
  ) {
    updateArticle(
      id: $id
      title: $title
      subtitle: $subtitle
      author: $author
      email: $email
      abstract: $abstract
      article: $article
      isAutosave: $isAutosave
    ) {
      status
      message
    }
  }
`;

// This is the only place InitializedMDXEditor is imported directly.
import dynamic from "next/dynamic";
import { Suspense, forwardRef } from "react";
const Editor = dynamic(() => import("@/components/issue/article/editor"), {
  // Make sure we turn SSR off
  ssr: false,
});

// This is what is imported by other components. Pre-initialized with plugins, and ready
// to accept other props, including a ref.
import type { MDXEditorMethods, MDXEditorProps } from "@mdxeditor/editor";
interface TitleType {
  title: string;
}
const ForwardRefEditor = forwardRef<
  MDXEditorMethods,
  MDXEditorProps & TitleType
>((props, ref) => <Editor {...props} editorRef={ref} />);

// TS complains without the following line
ForwardRefEditor.displayName = "ForwardRefEditor";

export default function Article({
  params: { locale, id, articleId },
}: {
  params: { locale: string; id: string; articleId: string };
}) {
  const t = useI18n();

  const [ready, setReady] = useState<boolean>(false);
  const [key, setKey] = useState<number>(1);
  const [title, setTitle] = useState<string>("");
  const [subtitle, setSubtitle] = useState<string>("");
  const [author, setAuthor] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [abstract, setAbstract] = useState<string>("");
  const [article, setArticle] = useState<string>("");

  /*
   * We need to have all values in Refs because autosave will call save(), which
   * will only have the initial values by default. useRef will allow the
   * save() function to access current values instead of initial ones.
   */
  const titleRef = useRef<string>();
  const subtitleRef = useRef<string>();
  const authorRef = useRef<string>();
  const emailRef = useRef<string>();
  const abstractRef = useRef<string>();
  const articleRef = useRef<string>();

  titleRef.current = title;
  subtitleRef.current = subtitle;
  authorRef.current = author;
  emailRef.current = email;
  abstractRef.current = abstract;
  articleRef.current = article;

  const [showError, setShowError] = useState<boolean>(false);
  const [error, setError] = useState<string>("");
  const [status, setStatus] = useState<string>("");

  const [lastSaved, setLastSaved] = useState<Date>(new Date());
  const [saving, setSaving] = useState<boolean>(false);
  const [iterator, setIterator] = useState<number>(0);

  const {
    data,
    loading,
    refetch: refetchArticle,
  } = useQuery(getArticleQuery, {
    variables: {
      id: Number(articleId),
    },
    fetchPolicy: "network-only",
  });
  const { data: revisions, refetch } = useQuery(getRevisionsQuery, {
    variables: {
      articleId: Number(articleId),
    },
  });
  const [updateArticle] = useMutation(updateArticleMutation);

  useEffect(() => {
    if (!data) return;
    if (ready) return;
    const { title, subtitle, author, email, abstract, article } =
      data.getArticle;
    setTitle(title);
    setSubtitle(subtitle);
    setAuthor(author);
    setEmail(email);
    setAbstract(abstract);
    setArticle(article);

    setReady(true);
    setKey(key + 1);
  }, [data]);

  useEffect(() => {
    const interval = setInterval(
      () => setIterator(new Date().getSeconds()),
      10000,
    );
    return () => {
      clearInterval(interval);
    };
  }, []);

  useEffect(() => {
    const interval = setInterval(async () => await save(true), 120000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  useEffect(() => {
    const callback = (event: KeyboardEvent) => {
      // event.metaKey - pressed Command key on Macs
      // event.ctrlKey - pressed Control key on Linux or Windows
      if ((event.metaKey || event.ctrlKey) && event.code === "KeyS") {
        event.preventDefault();
        save(false);
      }
    };
    document.addEventListener("keydown", callback);
    return () => {
      document.removeEventListener("keydown", callback);
    };
  }, []);

  const save = async (isAutosave: boolean) => {
    setSaving(true);
    const res = await updateArticle({
      variables: {
        id: Number(articleId),
        title: titleRef.current,
        subtitle: subtitleRef.current,
        author: authorRef.current,
        email: emailRef.current,
        abstract: abstractRef.current,
        article: articleRef.current,
        isAutosave,
      },
    });

    if (res.data.updateArticle.status !== "OK") {
      // something went wrong
      setShowError(true);
      setError(res.data.updateArticle.message);
      setStatus(res.data.updateArticle.status);
      setSaving(false);
      return;
    }

    setLastSaved(new Date());
    setShowError(false);
    setSaving(false);
    refetch();
    return;
  };

  return (
    <div>
      <Title>
        <div className="grid grid-cols-1 gap-2 sm:grid-cols-2">
          <div className="overflowX-auto col-span-full">
            <input
              type="text"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              placeholder={t("titlePlaceholder")}
              className="w-full bg-transparent py-2 text-left text-4xl font-black sm:p-2 sm:text-center sm:text-6xl dark:border-stone-400"
            />
          </div>
          <div className="overflowX-auto col-span-full mb-8">
            <input
              type="text"
              value={subtitle}
              onChange={(e) => setSubtitle(e.target.value)}
              placeholder={t("subtitlePlaceholder")}
              className="w-full bg-transparent py-1 text-left text-2xl font-bold text-stone-500 sm:p-1 sm:text-center sm:text-4xl dark:border-stone-400 dark:text-stone-400"
            />
          </div>
          <div>
            <input
              type="text"
              value={author}
              onChange={(e) => setAuthor(e.target.value)}
              placeholder={t("authorPlaceholder")}
              className="w-full bg-transparent text-left text-left text-lg font-semibold sm:text-2xl dark:border-stone-400"
            />
          </div>
          <div>
            <input
              type="text"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              placeholder={t("emailPlaceholder")}
              className="w-full bg-transparent text-left text-lg font-semibold sm:text-right sm:text-right sm:text-2xl dark:border-stone-400"
            />
          </div>
        </div>
      </Title>
      <div className="mx-auto w-full max-w-[1200px] px-6 py-6 text-black dark:text-stone-100">
        <SessionProvider>
          <div className="flex w-full flex-col items-center justify-evenly gap-4 sm:flex-row">
            <div className="w-fit">
              <StatusSelection
                status={data?.getArticle.status}
                refetch={refetchArticle}
                article={data?.getArticle}
              />
            </div>
            <div className="w-fit">
              <EditIssue
                articleId={Number(articleId)}
                issueId={Number(id)}
                article={data?.getArticle}
              />
            </div>
            {data?.getArticle && (
              <Link
                href={`/issue/${data.getArticle.issue.id}/article/${data.getArticle.id}`}
                className="rounded-md bg-black px-3 py-2 font-semibold text-white shadow-md dark:bg-white dark:text-black"
              >
                {t("viewRendered")}
              </Link>
            )}
          </div>
        </SessionProvider>

        <Separator />

        <div className="mx-auto w-fit">
          <Autosave
            save={save}
            saving={saving}
            lastSaved={lastSaved}
            key={iterator}
            locale={locale}
            ready={ready}
          />
        </div>
        {showError && (
          <Alert title={status} icon={<IconX />} error={true} className="mt-4">
            {error}
          </Alert>
        )}

        <ForwardRefEditor
          title={t("abstract")}
          markdown={abstract}
          onChange={(e) => setAbstract(e)}
          key={key}
        />
        <ForwardRefEditor
          title={t("article")}
          markdown={article}
          onChange={(e) => setArticle(e)}
          key={-key}
        />

        <Revisions
          revisions={revisions?.getRevisions}
          locale={locale}
          editMode={true}
          setAbstract={(value: string) => {
            setAbstract(value);
            setKey(key + 1);
          }}
          setArticle={(value: string) => {
            setArticle(value);
            setKey(key + 1);
          }}
        />
      </div>
    </div>
  );
}
