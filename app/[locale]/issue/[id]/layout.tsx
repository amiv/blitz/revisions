import type { Metadata, ResolvingMetadata } from "next";

import Unauthorized from "@/components/unauthorized";

import { isAllowedServer } from "@/utils/authorization";

type Props = {
  params: { locale: "de" | "en" };
};

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata,
): Promise<Metadata> {
  const locale = params.locale;

  return locale === "de"
    ? {
        title: "Ausgabe | Revisions - Redaktionssystem des blitz",
        description:
          "Redaktionssystem des blitz, der Fachzeitschrift des AMIV an der ETH",
      }
    : {
        title: "Issue | Revisions - Editorial System of blitz",
        description:
          "Editorial System of blitz, the student magazine of AMIV at ETH",
      };
}

export default function IssueLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  if (!isAllowedServer("issue.view", undefined, undefined))
    return <Unauthorized target="issue.view" />;
  return <>{children}</>;
}
