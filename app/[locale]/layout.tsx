import type { Metadata, ResolvingMetadata } from "next";
import { Inter } from "next/font/google";

const inter = Inter({ subsets: ["latin"] });

import { I18nProviderClient } from "@/locales/client";
import { ApolloWrapper } from "@/graphql/ApolloWrapper";

import Navbar from "@/components/navbar";
import Footer from "@/components/footer";

type Props = {
  params: { locale: "de" | "en" };
};

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata,
): Promise<Metadata> {
  const locale = params.locale;

  return locale === "de"
    ? {
        title: "Revisions - Redaktionssystem des blitz",
        description:
          "Redaktionssystem des blitz, der Fachzeitschrift des AMIV an der ETH",
      }
    : {
        title: "Revisions - Editorial System of blitz",
        description:
          "Editorial System of blitz, the student magazine of AMIV at ETH",
      };
}

export default function RootLayout({
  children,
  params: { locale },
}: Readonly<{
  children: React.ReactNode;
  params: { locale: string };
}>) {
  return (
    <html lang={locale}>
      <body
        className={`${inter.className} relative flex min-h-screen flex-col bg-white dark:bg-stone-800`}
      >
        <I18nProviderClient locale={locale}>
          <ApolloWrapper>
            <Navbar locale={locale} />
            <main className="mt-4 flex-1">{children}</main>
            <Footer locale={locale as "de" | "en"} />
          </ApolloWrapper>
        </I18nProviderClient>
      </body>
    </html>
  );
}
