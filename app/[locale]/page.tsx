"use client";

import { SessionProvider } from "next-auth/react";

import { IconBookOff } from "@tabler/icons-react";

import { useI18n } from "@/locales/client";

import NextIssueCard from "@/components/main/nextIssueCard";
import Alert from "@/components/alert";
import AddIssue from "@/components/main/addIssue";

import { IssueType } from "@/types/types";

import { gql, useQuery } from "@apollo/client";

const getNextIssuesQuery = gql`
  query getNextIssues {
    getNextIssues {
      id
      name
      articles {
        title
        author
      }
      publicationDate
      deadline
      semester
      number
    }
  }
`;

export default function Home({
  params: { locale },
}: {
  params: { locale: string };
}) {
  const t = useI18n();
  const { data: nextIssues, refetch } = useQuery(getNextIssuesQuery);

  const nextIssue = nextIssues?.getNextIssues[0] || undefined;
  const otherIssues = nextIssues?.getNextIssues.slice(1) || undefined;

  return (
    <SessionProvider>
      <div>
        <div className="relative shadow-md">
          <div
            className="bg-grid-slate-900 absolute inset-0 -z-10 block h-full dark:hidden"
            style={{
              backgroundSize: "20px 20px",
              backgroundImage:
                "radial-gradient(circle, #cccccc 1px, rgba(0, 0, 0, 0) 1px)",
              maskImage: "linear-gradient(to bottom, transparent, black)",
              WebkitMaskImage: "linear-gradient(to bottom, transparent, black)",
            }}
          />
          <div
            className="bg-grid-slate-900 absolute inset-0 -z-10 hidden h-full dark:block"
            style={{
              backgroundSize: "20px 20px",
              backgroundImage:
                "radial-gradient(circle, #444444 1px, rgba(0, 0, 0, 0) 1px)",
              maskImage: "linear-gradient(to bottom, transparent, black)",
              WebkitMaskImage: "linear-gradient(to bottom, transparent, black)",
            }}
          />
          <div className="mx-auto w-full max-w-[1300px] px-6 py-12">
            {nextIssue ? (
              <NextIssueCard
                t={t}
                issue={nextIssue}
                locale={locale}
                showLogo={true}
                showArticles={true}
                showViewButton={true}
                showNextIssue={true}
              />
            ) : (
              <>
                <Alert
                  title={t("noNextIssues")}
                  icon={<IconBookOff stroke={1.5} />}
                >
                  {t("noNextIssuesText")}
                </Alert>
              </>
            )}

            <AddIssue refetch={refetch} />
          </div>
        </div>

        <div className="mx-auto w-full max-w-[1200px] px-6 py-12">
          {otherIssues && (
            <>
              {otherIssues.map((oi: IssueType, i: number) => (
                <NextIssueCard t={t} issue={oi} locale={locale} key={i} />
              ))}
            </>
          )}
        </div>
      </div>
    </SessionProvider>
  );
}
