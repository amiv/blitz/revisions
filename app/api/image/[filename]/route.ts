// @ts-nocheck
import fs from "fs";
import path from "path";

import prisma from "@/lib/prisma";

//import { getToken } from "next-auth/jwt";
import auth from "@/lib/auth";

import { isAllowedServer } from "@/utils/authorization";

export async function GET(request: Request, { params }) {
  const filename = params.filename;
  //const token = await getToken({ req: request });
  const session = await auth();
  if (!session)
    return Response.json({
      status: 401,
      statusText: "Not Logged In",
    });
  const username = session.username;

  const user = await prisma.user.findUnique({
    where: {
      username,
    },
  });
  if (!isAllowedServer("article.view", user, undefined))
    return Response.json({
      status: 401,
      statusText: "You don't have the permission to view this resource",
    });

  var filePath = path.join(process.cwd(), "images", filename);
  const imageBuffer = fs.readFileSync(filePath);

  const res = new Response(imageBuffer, {
    status: 200,
    statusText: "OK",
  });

  const mime = filename.split(".")[filename.split(".").length - 1];

  res.headers.append("Content-Type", `image/${mime}`);

  return res;
}
