// @ts-nocheck
import { NextResponse } from "next/server";
import fs from "fs";
import path from "path";
import { v4 as uuidv4 } from "uuid";

export async function POST(request: Request) {
  const formData = await request.formData();
  const file = formData.get("image");

  if (!file)
    return Response.json({ status: "400", message: "No file provided" });

  const destinationDirPath = path.join(process.cwd(), "images");
  const extension = file.name.split(".")[file.name.split(".").length - 1];
  const newFilename = uuidv4() + "." + extension;

  const fileArrayBuffer = await file.arrayBuffer();

  await fs.writeFile(
    path.join(destinationDirPath, newFilename),
    Buffer.from(fileArrayBuffer),
    (err) => {
      if (err) console.error(err);
    },
  );

  const url = `${process.env.NEXTAUTH_URL}/api/image/${newFilename}`;
  return Response.json({ url });
}
