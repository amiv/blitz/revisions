import type { Metadata } from "next";

import "./globals.css";

export const metadata: Metadata = {
  title: "Revisions - Redaktionssystem des blitz",
  description:
    "Redaktionssystem des blitz, der Fachzeitschrift des AMIV an der ETH",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <>
      <head>
        <link rel="icon" href="/favicon.ico" sizes="any" />
        <link
          rel="icon"
          href="/icon?<generated>"
          type="image/<generated>"
          sizes="<generated>"
        />
        <link
          rel="apple-touch-icon"
          href="/apple-icon?<generated>"
          type="image/<generated>"
          sizes="<generated>"
        />
      </head>
      {children}
    </>
  );
}
