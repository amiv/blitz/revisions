import { getServerSession } from "next-auth";
import authOptions from "./authOptions";

export default async function auth() {
  return await getServerSession(authOptions);
}
