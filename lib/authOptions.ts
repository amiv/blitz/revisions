// @ts-nocheck

import type { OIDCConfig } from "@auth/core/providers";

import axios from "axios";

import updateUser from "@/utils/updateUser";

const authOptions = {
  providers: [
    {
      id: "amiv",
      name: "AMIVAPI",
      type: "oauth",
      issuer: `${process.env.API_URL || "https://api-dev.amiv.ethz.ch"}/oauth`,
      authorization: {
        url: `${process.env.API_URL || "https://api-dev.amiv.ethz.ch"}/oauth`,
        params: {
          grant_type: "implicit",
          response_type: "token",
          scope: "openid email profile",
        },
      },
      token: {
        request: async (context) => {
          const token = context.params.access_token;
          const session = await axios.get(
            `${process.env.API_URL || "https://api-dev.amiv.ethz.ch"}/sessions/${token}`,
            {
              headers: {
                Authorization: token,
              },
            },
          );
          const userId = session.data.user;

          return {
            tokens: [{ access_token: userId }, { access_token: token }],
          };
        },
      },
      userinfo: {
        request: async (context) => {
          const userId = context.tokens["0"].access_token;
          const token = context.tokens["1"].access_token;

          const userObj = await axios.get(
            `${process.env.API_URL || "https://api-dev.amiv.ethz.ch"}/users/${userId}`,
            {
              headers: {
                Authorization: token,
              },
            },
          );

          const groups = await axios.get(
            `${process.env.API_URL || "https://api-dev.amiv.ethz.ch"}/groupmemberships?where={"user":"${userId}"}`,
            {
              headers: {
                Authorization: token,
              },
            },
          );

          const groupIds = groups.data._items.map((g) => g.group);

          const isAdmin = groupIds.includes(process.env.BLITZ_ADMIN_GROUP);

          const user = {
            id: userId,
            username: userObj.data.nethz,
            firstname: userObj.data.firstname,
            lastname: userObj.data.lastname,
            email: userObj.data.email,
            amivId: userId,
          };

          const userEntity = await updateUser(user);

          const isMember = userEntity.roles.split(",").length > 0;

          return {
            id: userId,
            username: userObj.data.nethz,
            firstname: userObj.data.firstname,
            lastname: userObj.data.lastname,
            email: userObj.data.email,
            amivId: userId,
            isAdmin,
            isMember,
          };
        },
      },
      profile(profile) {
        return profile;
      },
      clientId: process.env.CLIENT_ID,
      clientSecret: process.env.CLIENT_SECRET,
    } satisfies OIDCConfig,
  ],
  pages: {
    signIn: "/login",
  },
  secret: process.env.NEXTAUTH_SECRET,
  callbacks: {
    session: async ({ session, token }) => {
      if (!token) return session;
      return {
        ...session.user,
        username: token.username,
        firstname: token.firstname,
        lastname: token.lastname,
        email: token.email,
        amivId: token.amivId,
        isAdmin: token.isAdmin,
        isMember: token.isMember,
      };
    },
    jwt: async ({ token, profile }) => {
      if (!profile) return token;
      return {
        email: token.email,
        firstname: profile.firstname,
        lastname: profile.lastname,
        id: profile.id,
        username: profile.username,
        name: profile.firstname + " " + profile.lastname,
        amivId: profile.amivId,
        isAdmin: profile.isAdmin,
        isMember: profile.isMember,
      };
    },
  },
};

export default authOptions;
