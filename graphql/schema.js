import { DateTime } from "graphql-scalars";

export const typeDefs = `
  scalar DateTime

  type Issue {
    id: Int
    name: String
    articles: [Article]
    publicationDate: DateTime
    deadline: DateTime
    semester: String
    number: String
  }

  type Article {
    id: Int
    title: String
    subtitle: String
    author: String
    email: String
    abstract: String
    article: String
    issue: Issue
    user: User
    isAutosave: Boolean
    revisions: [Revision]
    status: Status
    updatedAt: DateTime
    category: Category
  }

  type Revision {
    id: Int
    timestamp: DateTime
    article: Article
    user: User
    coAbstract: String
    coArticle: String
    diffArticle: String
    diffAbstract: String
    isAutosave: Boolean
    status: Status
  }

  enum Status {
    COMPOSITION
    LECTORATE
    LAYOUT
    FINISHED
  }

  enum Category {
    BLITZ
    AMIV
    TOPIC
    STUDIES
  }

  type User {
    username: String
    firstname: String
    lastname: String
    email: String
    articles: [Article]
    roles: String
  }

  type Response {
    status: String
    message: String
  }

  type Breadcrumb {
    label: String
    path: String
    translate: Boolean
  }

  type Query {
    getNextIssues: [Issue]
    getIssueById(id: Int): Issue
    getUsers: [User]
    getUser: User
    getArticle(id: Int): Article
    getRevisions(articleId: Int): [Revision]
    getBreadcrumbs(pathname: String): [Breadcrumb]
    getStatus(articleId: Int): Status
  }

  type Mutation {
    addIssue(title: String, semester: String, number: String, publicationDate: DateTime, deadline: DateTime): Response
    updateUser(username: String, roles: String): Response
    addArticle(issueId: Int): Response
    updateArticle(id: Int, title: String, subtitle: String, author: String, email: String, abstract: String, article: String, isAutosave: Boolean): Response
    updateStatus(articleId: Int, status: Status): Response
    moveArticle(issueId: Int, articleId: Int): Response
    deleteArticle(articleId: Int): Response
  }
`;
