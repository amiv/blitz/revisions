import prisma from "../lib/prisma";

import dayjs from "dayjs";

import { diffWords, createPatch } from "diff";

import { isInFuture } from "@/utils/dates";
import updateUser from "@/utils/updateUser";
import getBreadcrumbs from "@/utils/getBreadcrumbs";
import { isAllowed } from "@/utils/authorization";

export const resolvers = {
  Query: {
    getNextIssues: async (_, __, { session }) => {
      if (!isAllowed("issue.view", session, undefined, undefined)) return [];

      const issues = await prisma.issue.findMany({
        include: {
          articles: {
            select: {
              title: true,
              author: true,
            },
          },
        },
        orderBy: [{ publicationDate: "asc" }],
      });

      return issues.filter((i) => isInFuture(i.publicationDate));
    },
    getIssueById: async (_, { id }, { session }) => {
      if (!isAllowed("issue.view", session, undefined, undefined)) return null;

      const user = session?.username
        ? await prisma.user.findUnique({
            where: {
              username: session.username,
            },
          })
        : undefined;

      return await prisma.issue.findUnique({
        where: {
          id,
        },
        include: {
          articles: {
            select: {
              title: true,
              author: true,
              id: true,
              status: true,
              user: isAllowed("article.view", session, user, undefined),
            },
          },
        },
      });
    },
    getUsers: async (_, __, { session }) => {
      if (!isAllowed("user.view", session, undefined, undefined)) return [];

      const users = await prisma.user.findMany({
        where: {
          roles: {
            not: "",
          },
        },
        orderBy: [{ lastname: "asc" }, { username: "asc" }],
      });

      return users;
    },
    getUser: async (_, __, { session }) => {
      if (!isAllowed("user.view", session, undefined, undefined)) return null;

      return await prisma.user.findUnique({
        where: {
          username: session?.username || null,
        },
      });
    },
    getArticle: async (_, { id }, { session }) => {
      if (!session?.username) return null;
      const user = await prisma.user.findUnique({
        where: { username: session?.username || null },
      });

      if (!isAllowed("article.view", session, user, undefined)) return null;

      const article = await prisma.article.findUnique({
        where: {
          id,
        },
        include: {
          issue: true,
          user: true,
          revisions: {
            include: {
              user: true,
            },
          },
        },
      });

      return article;
    },
    getRevisions: async (_, { articleId }, { session }) => {
      if (!session?.username) return null;
      const user = await prisma.user.findUnique({
        where: { username: session.username },
      });

      if (!isAllowed("article.view", session, user, undefined)) return null;

      const article = await prisma.article.findUnique({
        where: {
          id: articleId,
        },
        include: {
          revisions: {
            include: {
              user: true,
            },
            orderBy: [{ timestamp: "desc" }],
          },
        },
      });

      return article.revisions;
    },
    getBreadcrumbs: async (_, { pathname }) => {
      return await getBreadcrumbs(pathname);
    },
    getStatus: async (_, { articleId }) => {
      const article = await prisma.article.findUnique({
        where: {
          id: articleId,
        },
      });

      return article.status;
    },
  },
  Mutation: {
    addIssue: async (
      _,
      { title, semester, number, publicationDate, deadline },
      { session },
    ) => {
      if (!isAllowed("issue.create", session, undefined, undefined))
        return {
          status: "Unauthorized",
          message:
            "You don't have the necessary permissions to perform this operation",
        };

      const newPublicationDate = dayjs(publicationDate)
        .hour(0)
        .minute(0)
        .second(0)
        .millisecond(0);
      const newDeadline = dayjs(deadline)
        .hour(23)
        .minute(59)
        .second(0)
        .millisecond(0);

      await prisma.issue.create({
        data: {
          name: title,
          semester,
          number,
          publicationDate: newPublicationDate,
          deadline: newDeadline,
        },
      });

      return {
        status: "OK",
        message: "Issue was Added.",
      };
    },
    deleteArticle: async (_, { id }, { session }) => {
      if (!isAllowed("article.delete", session, undefined, undefined))
        return {
          status: "Unauthorized",
          message:
            "You don't have the necessary permissions to perform this operation",
        };
      await prisma.article.delete({
        where: {
          id
        }
      })
    },
    updateUser: async (_, { username, roles }, { session }) => {
      if (!isAllowed("users.edit", session, undefined, undefined))
        return {
          status: "Unauthorized",
          message:
            "You don't have the necessary permissions to perform this operation",
        };

      const user = await prisma.user.findUnique({
        where: {
          username,
        },
      });

      if (!user) {
        await prisma.user.create({
          data: {
            username,
            roles,
          },
        });
      } else {
        await prisma.user.update({
          where: {
            username,
          },
          data: {
            roles,
          },
        });
      }

      return {
        status: "OK",
        message: "User was added/updated",
      };
    },
    addArticle: async (_, { issueId }, { session }) => {
      const user = await prisma.user.findUnique({
        where: { username: session.username },
      });

      if (!isAllowed("article.create", session, user, undefined))
        return {
          status: "Unauthorized",
          message:
            "You don't have the necessary permissions to perform this operation",
        };

      const article = await prisma.article.create({
        data: {
          username: session.username,
          issueId,
          abstract: "",
          article: "",
        },
      });

      return {
        status: "OK",
        message: article.id,
      };
    },
    updateArticle: async (
      _,
      { id, title, subtitle, author, email, abstract, article, isAutosave },
      { session },
    ) => {
      const user = await prisma.user.findUnique({
        where: { username: session.username },
      });

      const art = await prisma.article.findUnique({
        where: {
          id,
        },
        include: {
          user: true,
        },
      });

      if (!isAllowed("article.edit", session, user, art.user.username))
        return {
          status: "Unauthorized",
          message:
            "You don't have the necessary permissions to perform this operation",
        };

      /*
       * DIFF
       */
      const coAbs = diffWords(art.abstract, abstract);
      const coArt = diffWords(art.article, article);
      const diffAbs = createPatch(art.title, art.abstract, abstract);
      const diffArt = createPatch(art.title, art.article, article);

      if (
        coAbs.length !== 1 ||
        coAbs[0].added ||
        coAbs[0].removed ||
        coArt.length !== 1 ||
        coArt[0].added ||
        coArt[0].removed
      ) {
        // article or abstract changed
        await prisma.revision.create({
          data: {
            articleId: art.id,
            username: session.username,
            coAbstract: JSON.stringify(coAbs),
            coArticle: JSON.stringify(coArt),
            diffAbstract: diffAbs,
            diffArticle: diffArt,
            isAutosave,
            status: art.status,
          },
        });
      }

      await prisma.article.update({
        where: {
          id,
        },
        data: {
          title,
          subtitle,
          author,
          email,
          abstract,
          article,
        },
      });

      return {
        status: "OK",
        message: "Article was successfully updated.",
      };
    },
    updateStatus: async (_, { articleId, status }, { session }) => {
      const article = await prisma.article.findUnique({
        where: {
          id: articleId,
        },
        include: { user: true },
      });

      const allowed = (() => {
        if (session.isAdmin) return true;
        if (status === "COMPOSITION")
          return isAllowed(
            "article.edit",
            session,
            user,
            article.user.username,
          );
        if (status === "LECTORATE")
          return (
            isAllowed("article.edit", session, user, article.user.username) ||
            isAllowed("article.lectorate", session, user, article.user.username)
          );
        if (status === "LAYOUT")
          return (
            isAllowed(
              "article.lectorate",
              session,
              user,
              article.user.username,
            ) ||
            isAllowed("article.layout", session, user, article.user.username)
          );
        if (status === "FINISHED")
          return isAllowed(
            "article.layout",
            session,
            user,
            article.user.username,
          );
        return false;
      })();

      if (!allowed)
        return {
          status: "Unauthorized",
          message:
            "You don't have the necessary permissions to perform this operation",
        };

      await prisma.article.update({
        where: {
          id: articleId,
        },
        data: {
          status,
        },
      });

      return {
        status: "OK",
        message: "Status was successfully set.",
      };
    },
    moveArticle: async (_, { issueId, articleId }, { session }) => {
      const user = await prisma.user.findUnique({
        where: { username: session.username },
      });

      const art = await prisma.article.findUnique({
        where: {
          id: articleId,
        },
        include: {
          user: true,
        },
      });

      if (!isAllowed("article.move", session, user, art.user.username))
        return {
          status: "Unauthorized",
          message:
            "You don't have the necessary permissions to perform this operation",
        };

      await prisma.article.update({
        where: {
          id: articleId,
        },
        data: {
          issueId,
        },
      });

      return {
        status: "OK",
        message: "Article was moved",
      };
    },
  },
  
};
