FROM node:21

ENV TZ="Europe/Zurich"

RUN mkdir -p /revisions
RUN mkdir -p /revisions/images
WORKDIR /revisions

COPY package.json .
COPY package-lock.json .
COPY prisma .
RUN npm install

COPY . .
RUN npx prisma generate
RUN npm run build

EXPOSE 3000

CMD ["bash", "entrypoint.sh"]
