import NextAuth from "next-auth";

declare module "next-auth" {
  /**
   * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
   */
  interface Session {
    username: string;
    name: string;
    firstname: string;
    lastname: string;
    isMember: boolean;
    isAdmin: boolean;
    amivId: string;
    email: string;
  }
}
