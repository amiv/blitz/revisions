export interface SessionType {
  name: string;
  email: string;
  username: string;
  firstname: string;
  lastname: string;
  amivId: string;
  isAdmin: boolean;
  isMember: boolean;
}

export interface IssueType {
  id: number;
  name: string;
  articles: ArticleType[];
  publicationDate: string;
  deadline: string;
  semester: string;
  number: string;
}

export interface ArticleType {
  id: number;
  title: string;
  subtitle: string;
  author: string;
  email: string;
  abstract: string;
  article: string;
  issue: IssueType;
  user: UserType;
  isAutosave: boolean;
  status: "COMPOSITION" | "LECTORATE" | "LAYOUT" | "FINISHED";
  category: "BLITZ" | "AMIV" | "TOPIC" | "STUDIES";
  updatedAt: string;
}

export interface RevisionType {
  id: number;
  timestamp: string;
  article: ArticleType;
  user: UserType;
  coAbstract: string;
  coArticle: string;
  diffAbstract: string;
  diffArticle: string;
  isAutosave: boolean;
  status: "COMPOSITION" | "LECTORATE" | "LAYOUT" | "FINISHED";
}

export interface UserType {
  username: string;
  firstname: string;
  lastname: string;
  email: string;
  articles: ArticleType[];
  roles: string;
}

export type ValuePiece = Date | null;
export type Value = ValuePiece | [ValuePiece, ValuePiece];
